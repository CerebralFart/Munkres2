# Munkres2
NOTE: This project is still heavily a work in progress, functionality might be moved, changed or removed at any point.

## Installation
```
npm install
npm run server-build
npm run frontend-build
nodejs -r esm dist/App.js --adminCode <your admin code>
```
Now you can log in on `localhost:5000` with your admin code.

## Execution
1. *Configuration*:
   If so desired, you can configure groups before adding the projects. 
   1. Under the Projects tab in the Administration section, you can add, remove and edit projects.
      Each project has a name, an owner and an amount of places.
      Optionally, you can give the project an extended description.   
   2. Under the Groups tab in the Administration section, you can specify the amount of groups.
      Munkres2 will generate authentication codes for each group.
      These codes can be given to the groups.

2. *Open voting*:
   In the admin section, you can click the *Open voting* button, this allows groups to start indicating their preferences.
   Voting will stay open for 10 minutes.
   
   Also note that this will be blocked when there are insufficient places available.

3. *Completion*
   Once voting closes, Munkres2 will start computing the best assignment possible.
   Note that this process isn't fully deterministic, as there might be multiple equally good assignments.
   When the assignment is completed, it is shown to the groups, as well with some statistics.
   