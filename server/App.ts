import * as http from 'http';
import FileServer from "./FileServer";
import Config from "./Config";
import WSServer from "./WSServer";
import {log} from "./Util";

console.log('Launch config', Config);

const server = http.createServer();
FileServer.init(server);
WSServer.init(server);

server.listen(Config.port, () => {
	log(['server', 'started', 'port ' + Config.port])
});
