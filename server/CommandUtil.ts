import * as commandLineArgs from 'command-line-args';
import * as commandLineUsage from 'command-line-usage'

const configValid = (options, args) => {
	for (let i = 0; i < options.length; i++) {
		const option = options[i];
		const value = args[option.name];
		if (value === null || value === undefined) {
			console.log(`! Couldn\'t parse command line parameters, parameter ${option.name} was not found`);
			return false;
		}
	}
	return true;
};

export const parseCommand = (options) => {
	const opt = [
		...options,
		{
			name: 'help', alias: 'h', type: Boolean, defaultValue: false,
			description: 'Show this help message'
		}
	];

	const args = commandLineArgs(opt);

	if (args.help || !configValid(options, args)) {
		console.log(commandLineUsage([
			{
				header: 'Options',
				optionList: opt
			}
		]));
		throw new CliArgError;
	}

	delete args.help;

	return args;
};

class CliArgError extends Error {
}

process.on('uncaughtException', (e) => {
	if (!(e instanceof CliArgError)) {
		console.warn(e);
	}
});