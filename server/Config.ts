import {parseCommand} from "./CommandUtil";

const defaultPort = process.env.port || process.env.PORT || 5000;

const options = [
	{
		name: 'port', alias: 'p', type: Number, defaultValue: defaultPort,
		description: 'Port to bind munkres2 to'
	}, {
		name: 'dev', type: Boolean, defaultValue: process.env.NODE_ENV !== 'production',
		description: 'Launch munkres2 in dev mode (no functionality yet)'
	}, {
		name: 'silent', type: Boolean, defaultValue: false,
		description: 'Reduce logging'
	}, {
		name: 'adminCode', type: Number,
		description: 'Code to login in to the admin area ({bold REQUIRED})'
	}
];

let args = {
	silent: false,
	port: -1,
	dev: false,
	adminCode: '2718'
};
let initd = false;

function getArgs() {
	if (!initd) {
		args = parseCommand(options);
		initd = true;
	}
	return args;
}

let config = {
	get port(): number {
		return getArgs().port
	},
	get dev(): boolean {
		return getArgs().dev
	},
	set silent(value) {
		args.silent = value;
		initd = true;
	},
	get silent(): boolean {
		return getArgs().silent
	},
	get adminCode(): string {
		return '' + getArgs().adminCode
	},
	get codeLength(): number {
		return getArgs().adminCode.length;
	},

	publicRoot: './build',
	selectionSize: 3
};
export default config;