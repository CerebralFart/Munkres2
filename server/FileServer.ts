import Config from "./Config";
import * as fs from 'fs';
import {log} from "./Util";
import * as express from "express";

const defaultFile = '/index.html';

class FileServer {
	app: express.Application;
	_fsCache: { [key: string]: string } = {};

	constructor() {
		this._fsCache[defaultFile] = 'App is still booting up.';
	}

	init(server) {
		this.app = express();
		server.on('request', this.app);

		this.app.get('/*', (req, res) => {
			res.write(this.get(req.params[0]));
			res.send();
		});
		setTimeout(() => this._initialize(), 0);
	}

	_initialize() {
		log(['fileserver', 'initializing']);
		this._loadDir('');
	}

	_loadDir(folder) {
		log(['fileserver', 'scanning', folder]);
		const publicRoot = Config.publicRoot;
		fs.readdir(publicRoot + folder, (err, files) => {
			files.forEach(file => {
				const path = folder + '/' + file;
				if (fs.lstatSync(publicRoot + path).isDirectory()) {
					this._loadDir(path);
				} else {
					fs.readFile(publicRoot + path, {encoding: 'utf-8'}, (err, data) => {
						log(['fileserver', 'loaded', path]);
						if (!err) {
							this._fsCache[path] = data;
						}
					});
				}
			})
		})
	}

	get(file) {
		file = '/' + file;
		log(['fileserver', 'request', file]);
		if (Object.keys(this._fsCache).indexOf(file) === -1) {
			file = defaultFile;
		}
		log(['fileserver', 'serving', file]);
		return this._fsCache[file];
	}
}

export default new FileServer();