import Group from "./Group";

class GroupFactoryClass {
	codeLookup: { [key: string]: Group } = {};
	nextIdx: number = 1;
	instances: Group[] = [];

	getInstances(): Group[] {
		return this.instances;
	}

	getByCode(code: string): Group {
		if (!this._isCodeInUse(code)) throw 'Groupcode not found';
		return this.codeLookup[code];
	}

	createNoCode(): Group {
		const usedCodes = this.instances.map(group => group.getCode());
		let suggestedCode = null;
		while (suggestedCode === null || usedCodes.indexOf(suggestedCode) > -1) {
			suggestedCode = '' + Math.floor(1000 + Math.random() * 9000);
		}
		return this.create(suggestedCode);
	}

	create(code: string): Group {
		if (this._isCodeInUse(code)) throw 'Groupcode in use';
		let group = new Group(this.nextIdx, code);
		this.instances.push(group);
		this.codeLookup[code] = group;
		this.nextIdx++;
		return group;
	}

	destroy(group: Group): void {
		let idx = this.instances.indexOf(group);
		if (idx >= 0) {
			this.instances.splice(idx, 1);
			delete this.codeLookup[group.getCode()];
		}
	}

	clear(): void {
		this.instances.forEach(GroupFactory.destroy)
	}

	_isCodeInUse(code: string): boolean {
		return Object.keys(this.codeLookup).indexOf(code) !== -1;
	}
}

const GroupFactory: GroupFactoryClass = new GroupFactoryClass();

export default GroupFactory;