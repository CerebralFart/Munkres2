import {WSUtil} from "../WSServer";
import Project from "../Project/Project";

class Group {
	id: number;
	code: string;
	preferences: number[] = []; //Array of project IDs
	ws: WebSocket[];
	publishable: boolean;
	assignment: Project;

	constructor(id: number, code: string) {
		this.id = id;
		this.code = code;
		this.preferences = [];
		this.ws = [];
		this.publishable = true;
		this.assignment = null;
	}

	addWs(ws: WebSocket): void {
		this.ws.push(ws);
	}

	setPreferences(preferences: number[]) {
		this.preferences = preferences;
		this.publish({
			command: 'preferences',
			response: this.preferences
		});
	}

	setAssignment(project: Project): void {
		this.assignment = project;
		this.publish({
			command: 'assignment',
			response: this.assignment.getId()
		});
	}

	getId(): number {
		return this.id;
	}

	getCode(): string {
		return this.code;
	}

	getPreferences(): number[] {
		return this.preferences;
	}

	getAssignment(): Project {
		return this.assignment;
	}

	private publish(data): void {
		if (this.publishable) {
			this.publishable = false;
			this.ws.forEach(ws => WSUtil.send(ws, data));
			setTimeout(() => this.publishable = true, 100);
		}
	}
}

export default Group;
