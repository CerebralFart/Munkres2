import * as munkres from 'munkres-js';
import ProjectFactory from "../Project/Factory";
import {log} from '../Util';
import Project from "../Project/Project";
import Group from "../Group/Group";
import GroupFactory from "../Group/Factory";
import Result from './Result';

export default class MunkresExecutor {
	projects: Array<[number, number]> = [];
	preferences: Array<[string, number[]]> = [];
	_reverseProjectLookup: number[];
	_projectMatrix: number[][] = [];
	_munkresMatrix: number[][] = [];


	loadProjects(): void {
		this.projects = [];
		this._reverseProjectLookup = [];
		const projects: Project[] = ProjectFactory.getInstances();
		for (let i = 0; i < projects.length; i++) {
			const project = projects[i];
			this.projects.push([
				project.getId(),
				project.getPlaces()
			]);
		}
		log(['munkres executor', 'projects', 'updated']);
		this.buildReverseLookup();
		this._queueCacheRebuild();
	}

	loadProjectsArray(projects: Array<[number, number]>): void {
		this.projects = projects;
		this.buildReverseLookup();
		this._queueCacheRebuild();
	}

	loadGroups(): void {
		const groupMapper = (group: Group): [string, number[]] => {
			return [
				group.getCode(),
				group.getPreferences()
			]
		};

		const groups = GroupFactory.getInstances();
		this.loadGroupsArray(groups.map(groupMapper));
	}

	loadGroupsArray(groups: Array<[string, number[]]>): void {
		this.preferences = groups;
		this._queueCacheRebuild();
		log(['munkres executor', 'groups', 'updated']);
	}

	isReady(): boolean {
		if (this.projects.length === 0 || this.preferences.length === 0) return false;
		const places: number = this.projects.map(project => project[1]).reduce((a, b) => a + b, 0);
		return places >= this.preferences.length;
	}

	_queueCacheRebuild(async?: boolean): void {
		const builder = () => {
			if (this.projects.length > 0) {
				this.buildReverseLookup();
			}
			if (this.projects.length > 0 && this.preferences.length > 0) {
				this.makeProjectMatrix();
				this.makeMunkresMatrix()
			}
		};

		if (async || async === undefined) {
			setTimeout(builder, 0);
		} else {
			builder();
		}
	}

	private buildReverseLookup(): void {
		this._reverseProjectLookup = [];
		for (let i = 0; i < this.projects.length; i++) {
			for (let j = 0; j < this.projects[i][1]; j++) {
				this._reverseProjectLookup.push(this.projects[i][0]);
			}
		}
		log(['munkres executor', 'projects', 'built reverse lookup']);
	}

	private makeProjectMatrix(): void {
		const n = this.preferences.length;
		const emptyLine: number[] = new Array(this.projects.length).fill(n + 1);
		for (let i = 0; i < this.preferences.length; i++) {
			this._projectMatrix.push(emptyLine.slice());
			for (let j = 0; j < this.preferences[i][1].length; j++) {
				this._projectMatrix[i][this.preferences[i][1][j]] = j;
			}
		}

		log(['munkres', 'executor', 'build project matrix']);
	}

	private makeMunkresMatrix(): void {
		this._munkresMatrix = new Array(this.preferences.length);
		for (let i = 0; i < this.preferences.length; i++) {
			this._munkresMatrix[i] = [];
			for (let j = 0; j < this._reverseProjectLookup.length; j++) {
				const projectId = this._reverseProjectLookup[j];
				this._munkresMatrix[i].push(this._projectMatrix[i][projectId]);
			}
		}

		log(['munkres', 'executor', 'build munkres matrix']);
	}

	compute(): Promise<Result> {
		return new Promise<Result>(resolve => {
			this._queueCacheRebuild(false);
			const munkresResult = munkres(this._munkresMatrix);
			log(['munkres', 'executor', 'calculated assignments']);
			resolve(new Result(munkresResult, this.preferences, this._reverseProjectLookup));
		})
	}

}