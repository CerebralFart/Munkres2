import ProjectFactory from "../Project/Factory";
import GroupFactory from "../Group/Factory";
import Config from "../Config";

class Result {
	result: [number, number][];
	preferences: [string, number[]][];
	assignments: number[] = [];
	_statsCache: { [key: number]: number } = {};

	constructor(munkresResult: [number, number][], preferences: [string, number[]][], lookup) {
		this.result = munkresResult;
		this.preferences = preferences;

		for (let i = 0; i < this.result.length; i++) {
			const assigned = this.result[i][1];
			this.assignments[i] = lookup[assigned];
		}
	}

	getRawResult(): [number, number][] {
		return this.result;
	}

	getAssignmentStats() {
		if (Object.keys(this._statsCache).length === 0) {
			for (let i = 0; i < this.assignments.length; i++) {
				const assigned = this.assignments[i];
				const preferences = this.preferences[i][1];

				let idx = preferences.indexOf(assigned);
				if (idx === -1) {
					idx = Config.selectionSize;
				}
				if (idx in this._statsCache) {
					this._statsCache[idx]++;
				} else {
					this._statsCache[idx] = 1;
				}
			}
		}
		return this._statsCache;
	}

	apply() {
		for (let i = 0; i < this.assignments.length; i++) {
			const project = ProjectFactory.getInstances()[this.assignments[i]];
			GroupFactory.getInstances()[i].setAssignment(project);
		}
	}

	getByProject(): { [key: number]: number[] } {
		const projects = ProjectFactory.getInstances();
		const groups = GroupFactory.getInstances();
		let result = {};
		for (let i = 0; i < projects.length; i++) {
			const project = projects[i];
			result[project.getId()] = groups.filter(group => group.getAssignment() === project).map(group => group.getId());
		}
		return result;
	}
}

export default Result;