import {genSalt, hash, compare} from "bcrypt";
import {Promise} from 'es6-promise';

export const hashPassword: Function = (password: string): Promise<string> => {
	return new Promise((resolve, reject) => {
		hash(password, 12, (err, hash) => {
			if (err) {
				reject(err);
			} else {
				resolve(hash);
			}
		})
	});

};

export const comparePassword: Function = (password: string, hash: string): Promise<boolean> => {
	return new Promise((resolve, reject) => {
		compare(password, hash, (err, isMatch) => {
			if (err) {
				reject(err);
			} else {
				resolve(isMatch);
			}
		})
	});
};
