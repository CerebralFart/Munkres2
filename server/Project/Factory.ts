import Project from "./Project";
import Publicator from "./Publicator";

class ProjectFactoryClass {
	instances: Project[];
	nextIdx: number;

	constructor() {
		this.instances = [];
		this.nextIdx = 0;
	}

	getInstances() {
		return this.instances;
	}

	getById(id: number) {
		const filtered = this.instances.filter(project => project.id === id);
		if (filtered.length === 0) throw 'Projectgroup is not defined';
		return filtered[0];
	}

	create(name: string, by: string): Project {
		if (by === '') by = name;
		this._checkName(name);

		const project = new Project(name, by);
		project.id = this.nextIdx;
		this.instances.push(project);
		this.nextIdx++;

		Publicator.update();

		return project;
	}

	destroy(project: Project): void {
		const idx = this.instances.indexOf(project);
		this.instances.splice(idx, 1);
	}

	_checkName(name): void {
		if (this._nameExists(name)) {
			throw new Error('Project with name ' + name + ' already exists');
		}
	}

	_nameExists(name): boolean {
		for (let i = 0; i < this.instances.length; i++) {
			const project = this.instances[i];
			if (project.getName() === name) {
				return true;
			}
		}
		return false;
	}

	clear(): void {
		this.instances.forEach(ProjectFactory.destroy);
	}
}

const ProjectFactory: ProjectFactoryClass = new ProjectFactoryClass();

export default ProjectFactory;