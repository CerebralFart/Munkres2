import ProjectFactoryClass from "./Factory";
import Publicator from "./Publicator";
import Group from "../Group/Group";

class Project {
	id: number;
	descriptionPre: string;
	description: string;
	placesPre: number;
	places: number;
	namePre: string;
	name: string;
	byPre: string;
	by: string;
	groups: Group[];

	constructor(name, by) {
		this.description = '';
		this.places = 1;
		this.name = name;
		this.by = by;
		this.groups = [];
	}

	setRollbackPoint(): void {
		this.descriptionPre = this.description;
		this.placesPre = this.places;
		this.namePre = this.name;
		this.byPre = this.by;
	}

	revert(): void {
		this.description = this.descriptionPre;
		this.places = this.placesPre;
		this.name = this.namePre;
		this.by = this.byPre;
	}

	setName(name): void {
		ProjectFactoryClass._checkName(name);
		this.name = name;
		Publicator.update();
	}

	setBy(by): void {
		this.by = by;
		Publicator.update();
	}

	setDescription(description): void {
		this.description = description;
		Publicator.update();
	}

	setPlaces(places): void {
		this.places = places;
		Publicator.update();
	}

	setGroups(groups: Group[]): void {
		this.groups = groups;
		Publicator.update();
	}

	addGroup(group: Group) {
		this.groups.push(group);
		Publicator.update();
	}

	getId(): number {
		return this.id;
	}

	getName(): string {
		return this.name;
	}

	getBy(): string {
		return this.by;
	}

	getDescription(): string {
		return this.description
	}

	getPlaces(): number {
		return this.places;
	}

	isValid(): boolean {
		if (this.places < 1) return false;
		if (this.by === '') return false;
		return this.name !== '';
	}
}

export default Project;