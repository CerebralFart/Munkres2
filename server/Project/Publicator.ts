import WSServer from "../WSServer";
import ProjectFactoryClass from "./Factory";

class PublicatorClass {
	publish: boolean;
	_updateCallback: Function;

	constructor() {
		this.publish = true;
		this._updateCallback = () => {
		};
	}

	enable(): void {
		this.publish = true;
	}

	disable(): void {
		this.publish = false;
	}

	update(): void {
		if (this.publish) {
			WSServer.broadcast('projects', ProjectFactoryClass.getInstances());
		}
		this._updateCallback();
	}
}

const Publicator: PublicatorClass = new PublicatorClass();

export default Publicator;