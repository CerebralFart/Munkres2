import ProjectFactory from "../Project/Factory";
import Publicator from "../Project/Publicator";
import GroupFactory from "../Group/Factory";
import Group from "../Group/Group";
import Project from "../Project/Project";
import MunkresExecutor from "../Munkres/Executor";
import {disableLog} from "../Util";
import Config from "../Config";
import {parseCommand} from "../CommandUtil";

if (false) {
	const {groupCount, projectCount, selectionSize, sampleSize, randomizePlaces, noProgress} = parseCommand([
		{
			name: 'groupCount', alias: 'g', type: Number, defaultValue: 5,
			description: 'Amount of groups in per trial'
		}, {
			name: 'projectCount', alias: 'p', type: Number, defaultValue: 6,
			description: 'Amount of projects available'
		}, {
			name: 'selectionSize', alias: 's', type: Number, defaultValue: 3,
			description: 'Amount of projects that are ranked'
		}, {
			name: 'sampleSize', alias: 't', type: Number, defaultValue: 10000,
			description: 'Amount of trials that are run'
		}, {
			name: 'randomizePlaces', alias: 'r', type: Boolean, defaultValue: false,
			description: 'Randomize available places each trial'
		}, {
			name: 'noProgress', alias: 'n', type: Boolean, defaultValue: false,
			description: 'Removes progress logging'
		}
	]);

	function getPlaces() {
		let places = new Array(projectCount);
		if (randomizePlaces) {
			places.fill(1);
			while (places.reduce((a, b) => a + b) != groupCount) {
				const idx = Math.floor(Math.random() * projectCount);
				places[idx]++;
			}
		} else {
			places.fill(Math.ceil(groupCount / projectCount));
		}
		return places;
	}

	Config.selectionSize = selectionSize;
	Publicator.disable();
	disableLog();

	let projects: Project[] = (new Array(projectCount))
		.fill('')
		.map((_, i) => {
			return ProjectFactory.create('' + i, '' + i);
		});
	let groups: Group[] = (new Array(groupCount))
		.fill('')
		.map(() => GroupFactory.createNoCode());

	function isValidConfig(config: string): boolean {
		let seen = [];
		for (let j = 0; j < config.length; j++) {
			let char = config.charAt(j);
			if (j % selectionSize === 0) {
				seen = [];
			}

			if (seen.indexOf(char) === -1) {
				seen.push(char);
			} else {
				return false;
			}
		}
		return true;
	}

	function generateValidChunks(): string[] {
		let validChunks = [];
		const max = Math.pow(projectCount, selectionSize);
		const zeroPrefix = '0'.repeat(selectionSize);
		for (let i = 0; i < max; i++) {
			let raw = zeroPrefix + i.toString(projectCount);
			raw = raw.substring(raw.length - selectionSize);
			if (isValidConfig(raw)) {
				validChunks.push(raw);
			}
		}
		return validChunks;
	}

	let validChunks = generateValidChunks();
	let groupAssignments = new Array(projectCount).fill(0);
	let trialAssignments = new Array(projectCount).fill(0);

	let completed = 0;

	console.log(`Running ${sampleSize} trials where ${groupCount} groups rank ${selectionSize} out of ${projectCount} projects with ${randomizePlaces ? 'random' : 'fixed'} available places`);

	const functor = () => {
		process.stdout.write('\r');
		if (completed > sampleSize - 1) {
			for (let i = 0; i < projectCount; i++) {
				let groupPrec = groupAssignments[i] * 100 / (groupCount * sampleSize);
				let trialPrec = trialAssignments[i] * 100 / sampleSize;
				console.log(`#${i + 1} pref.: ${groupAssignments[i]} groups total, ~${(groupAssignments[i] / sampleSize).toFixed(1)} per trial (${groupPrec.toFixed(2)}%)`);
				console.log(`          in ${trialAssignments[i]} trials (${trialPrec.toFixed(2)}%)`)
			}
		} else {
			if (!noProgress) {
				const perc = completed * 100 / sampleSize;
				process.stdout.write(`Completed ${perc.toFixed(2)}%`);
			}
			setTimeout(functor, 250);
		}
	};
	setTimeout(functor);

	const queue = i => {
		if (i >= sampleSize) return;
		setTimeout(() => {
			let executor = new MunkresExecutor();
			const places = getPlaces();
			projects.map((project, i) => {
				project.setPlaces(places[i]);
				return project;
			});
			executor.loadProjects();
			for (let j = 0; j < groupCount; j++) {
				let selection = validChunks[Math.floor(Math.random() * validChunks.length)];
				groups[j].setPreferences(selection.split("").map(e => parseInt(e, projectCount)));
			}
			executor.loadGroups();
			executor.compute().then(result => {
				let stats = result.getAssignmentStats();
				for (let j = 0; j < projectCount; j++) {
					groupAssignments[j] += stats[j] || 0;
					trialAssignments[j] += stats[j] === undefined ? 0 : 1;
				}
				completed++;
			});
			queue(i + 1);
		})
	};

	queue(0);
}

///---///

function isValidConfig(config: string, selectionSize): boolean {
	let seen = [];
	for (let j = 0; j < config.length; j++) {
		let char = config.charAt(j);
		if (j % selectionSize === 0) {
			seen = [];
		}

		if (seen.indexOf(char) === -1) {
			seen.push(char);
		} else {
			return false;
		}
	}
	return true;
}

function generateValidChunks(projectCount, selectionSize): string[] {
	let validChunks = [];
	const max = Math.pow(projectCount, selectionSize);
	const zeroPrefix = '0'.repeat(selectionSize);
	for (let i = 0; i < max; i++) {
		let raw = zeroPrefix + i.toString(projectCount);
		raw = raw.substring(raw.length - selectionSize);
		if (isValidConfig(raw, selectionSize)) {
			validChunks.push(raw);
		}
	}
	return validChunks;
}

function getPlaces(randomizePlaces, groupCount, projectCount) {
	let places = new Array(projectCount);
	if (randomizePlaces) {
		places.fill(1);
		while (places.reduce((a, b) => a + b) <= groupCount) {
			const idx = Math.floor(Math.random() * projectCount);
			places[idx]++;
		}
	} else {
		places.fill(Math.ceil(groupCount / projectCount));
	}
	return places;
}

export const getAssignmentStats = async (config): Promise<{ [name: string]: any }> => new Promise(resolve => {
	Config.selectionSize = config.selectionSize;
	Publicator.disable();
	disableLog();

	let validChunks = generateValidChunks(config.projectCount, config.selectionSize);
	let groupAssignments = new Array(config.projectCount).fill(0);
	let trialAssignments = new Array(config.projectCount).fill(0);

	// const functor = () => {
	// 	process.stdout.write('\r');
	// 	if (completed > sampleSize - 1) {
	// 		for (let i = 0; i < projectCount; i++) {
	// 			let groupPrec = groupAssignments[i] * 100 / (groupCount * sampleSize);
	// 			let trialPrec = trialAssignments[i] * 100 / sampleSize;
	// 			console.log(`#${i + 1} pref.: ${groupAssignments[i]} groups total, ~${(groupAssignments[i] / sampleSize).toFixed(1)} per trial (${groupPrec.toFixed(2)}%)`);
	// 			console.log(`          in ${trialAssignments[i]} trials (${trialPrec.toFixed(2)}%)`)
	// 		}
	// 	} else {
	// 		if (!noProgress) {
	// 			const perc = completed * 100 / sampleSize;
	// 			process.stdout.write(`Completed ${perc.toFixed(2)}%`);
	// 		}
	// 		setTimeout(functor, 250);
	// 	}
	// };
	// setTimeout(functor);

	const queue = i => {
		if (i >= config.sampleSize) {
			let obj: { groupAssignments: any, trialAssignments: any } = {
				groupAssignments,
				trialAssignments
			};
			resolve(obj);
			return;
		}
		setTimeout(() => {
			let groups = new Array(config.groupCount)
				.fill([])
				.map((_, idx): [string, number[]] => [
					'' + idx,
					validChunks[Math.floor(Math.random() * validChunks.length)].split("").map(e => parseInt(e, config.projectCount))
				]);
			let projects = getPlaces(config.randomizeProjectSizes, config.groupCount, config.projectCount)
				.map((places, idx): [number, number] => [
					idx,
					places
				]);
			let executor = new MunkresExecutor();
			executor.loadProjectsArray(projects);
			executor.loadGroupsArray(groups);
			executor.compute().then(result => {
				let stats = result.getAssignmentStats();
				for (let j = 0; j < config.projectCount; j++) {
					groupAssignments[j] += stats[j] || 0;
					trialAssignments[j] += stats[j] === undefined ? 0 : 1;
				}
				if (config.tick) config.tick();
			});
			queue(i + 1);
		})
	};

	queue(0);
});