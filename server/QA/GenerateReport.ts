import * as fs from 'fs';
import * as ProgressBar from 'progress';
import {getAssignmentStats} from './AssignmentTest';

const path = "./Munkres2-QAReport.md";
const groupCounts = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50];
const projectCounts = [3, 6, 9];
const selectionSizes = [3, 6];
const sampleSize = 100000;

const configs = (() => {
	let configs = [];

	projectCounts.map(projectCount => {
		selectionSizes.map(selectionSize => {
			if (selectionSize <= projectCount) {
				groupCounts.map(groupCount => {
					configs.push({
						groupCount,
						projectCount,
						selectionSize,
					});
				});
			}
		});
	});

	return configs;
})();
const batchSize = 3 * configs.length;
const getBar: Function = (() => {
	const titleLength = 35;
	const maxILen = ('' + batchSize).length;
	const prefix = '0'.repeat(maxILen);

	return (title: string, i: number, total: number) => {
		//pad title to 30 chars
		let paddedTitle = (title + ' '.repeat(titleLength)).substr(0, titleLength);

		//leftpad i to maxI length
		let idx = prefix + i;
		idx = idx.substr(idx.length - maxILen);

		let bar = new ProgressBar(`[${idx}/${batchSize}] ${paddedTitle} [:bar] :etas`, {
			head: '>',
			complete: '=',
			incomplete: ' ',
			stream: process.stdout,
			width: 40,
			renderThrottle: 500,
			total
		});
		return bar;
	};
})();
const getOrdinal = i => {
	let j = i % 10;
	let k = i % 100;
	if (j == 1 && k != 11) {
		return i + "st";
	}
	if (j == 2 && k != 12) {
		return i + "nd";
	}
	if (j == 3 && k != 13) {
		return i + "rd";
	}
	return i + "th";
};

if (!process.stdout.isTTY) {
	console.warn("Output doesn't seem to be TTY, logging might be funky.");
}

if (fs.existsSync(path)) {
	console.warn("Output file already exists, terminating");
	fs.truncateSync(path);//process.exit();
}

//Assignment stats (equal places)
(async () => {
	const file = fs.createWriteStream(path);

	//Write header info
	(() => {
		const date = new Date();
		file.write(`# Munkres2 - QA Report - ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}\n`);
		//TODO
		file.write("\n");
	})();

	//Assignment stats (equal places)
	await new Promise(async resolve => {
		file.write("## Assignment Statistics (fixed places)\n");
		for (let i = 0; i < configs.length; i++) {
			const config = configs[i];
			const bar = getBar(`Assignment statistics (f/g${config.groupCount}/p${config.projectCount}/s${config.selectionSize})`, i + 1, sampleSize);

			let {
				groupAssignments,
				trialAssignments
			} = await getAssignmentStats({
				...config, //GroupCount, ProjectCount, SelectionSize
				randomizeProjectSizes: false,
				sampleSize,
				tick: bar.tick.bind(bar)
			});

			file.write(`### ${config.groupCount} Groups, ${config.projectCount} Projects, ${config.selectionSize} Ranked\n`);
			for (let i = 0; i < config.projectCount; i++) {
				let groupPrec = groupAssignments[i] * 100 / (config.groupCount * sampleSize);
				let trialPrec = trialAssignments[i] * 100 / sampleSize;
				file.write(`${getOrdinal(i + 1)} pref.: ${groupAssignments[i]} groups total,\n`);
				file.write(`    ~${(groupAssignments[i] / sampleSize).toFixed(1)} per trial (${groupPrec.toFixed(2)}%),\n`);
				file.write(`    in ${trialAssignments[i]} trials (${trialPrec.toFixed(2)}%)\n\n`);
			}

		}
		resolve();
	});

	//Assignment stats (random places)
	await new Promise(async resolve => {
		file.write("## Assignment Statistics (random places)\n");
		for (let i = 0; i < configs.length; i++) {
			const config = configs[i];
			const bar = getBar(`Assignment statistics (r/g${config.groupCount}/p${config.projectCount}/s${config.selectionSize})`, i + configs.length + 1, sampleSize);

			let {
				groupAssignments,
				trialAssignments
			} = await getAssignmentStats({
				...config, //GroupCount, ProjectCount, SelectionSize
				randomizeProjectSizes: true,
				sampleSize,
				tick: bar.tick.bind(bar)
			});

			file.write(`### ${config.groupCount} Groups, ${config.projectCount} Projects, ${config.selectionSize} Ranked\n`);
			for (let i = 0; i < config.projectCount; i++) {
				let groupPrec = groupAssignments[i] * 100 / (config.groupCount * sampleSize);
				let trialPrec = trialAssignments[i] * 100 / sampleSize;
				file.write(`${getOrdinal(i + 1)} pref.: ${groupAssignments[i]} groups total,\n`);
				file.write(`    ~${(groupAssignments[i] / sampleSize).toFixed(1)} per trial (${groupPrec.toFixed(2)}%),\n`);
				file.write(`    in ${trialAssignments[i]} trials (${trialPrec.toFixed(2)}%)\n\n`);
			}

		}
		resolve();
	});

	//Relative assignment stats
	await new Promise(resolve => setTimeout(() => {
		console.log('RAS');
		file.write("## Relative assignment statistics\n");
		//TODO
		file.write("\n");
	}, 5000));

	file.end();
})();
