import WSServer from './WSServer';
import {log} from './Util';

let timeout;
const onChange = () => {
	log(['status', 'updated']);
	if (timeout !== null) {
		clearTimeout(timeout);
	}
	timeout = setTimeout(() => WSServer.broadcast('status', object), 0);
};

const object: { [key: string]: any } = {
	status: 'initializing',
	title: null,
	votingInterval: 600000
};

const handler: { [key: string]: Function } = {
	get(target, property, receiver) {
		try {
			return new Proxy(target[property], handler);
		} catch (err) {
			return Reflect.get(target, property, receiver);
		}
	},
	defineProperty(target, property, descriptor) {
		onChange();
		return Reflect.defineProperty(target, property, descriptor);
	}
};
const StatusObject = new Proxy(object, handler);

export default StatusObject;
