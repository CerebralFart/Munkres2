import test from 'ava';
import {WSUtil} from "../../WSServer";
import GroupFactory from "../../Group/Factory";
import ProjectFactory from "../../Project/Factory";
import Publicator from "../../Project/Publicator";

let group;

test.before(() => {
	Publicator.disable();
});

test.beforeEach(() => {
	group = GroupFactory.create('0000');
	group.addWs(null);
});

test.afterEach.always(() => {
	GroupFactory.destroy(group);
});

test.cb('publish_on_preferences_update', t => {
	t.plan(1);
	WSUtil.setCallback(() => {
		t.pass();
		t.end();
		return false;
	});

	group.setPreferences([1, 2]);
});

test.cb('publish_on_assignment_update', t => {
	t.plan(1);
	WSUtil.setCallback(() => {
		t.pass();
		t.end();
		return false;
	});

	let project = ProjectFactory.create('TEST', 'TEST');

	group.setAssignment(project);
});