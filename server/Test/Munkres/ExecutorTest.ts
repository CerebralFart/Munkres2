import test from 'ava';
import MunkresExecutor from "../../Munkres/Executor";
import * as Util from '../../Util';
import ProjectFactory from "../../Project/Factory";
import Publicator from "../../Project/Publicator";
import GroupFactory from "../../Group/Factory";

let executor;

test.before(() => {
	Util.disableLog();
	Publicator.disable();
});

test.beforeEach(() => {
	executor = new MunkresExecutor();
});

test('test_project_loading', t => {
	const p1 = ProjectFactory.create('TEMP', 'TEST');
	executor.loadProjects();
	t.true(executor.projects.length > 0);
	ProjectFactory.destroy(p1);
});

test('test_group_loading', t => {
	const g1 = GroupFactory.create('0');
	executor.loadGroups();
	t.true(executor.preferences.length > 0);
	GroupFactory.destroy(g1);
});

test('test_ready', t => {
	executor.loadProjectsArray([
		[0, 2],
		[1, 1]
	]);

	executor.loadGroupsArray([
		['1', [1, 2]],
		['2', [2, 1]]
	]);

	t.true(executor.isReady());
});

test('test_ready_no_projects', t => {
	const g1 = GroupFactory.create('0');
	executor.loadGroups();

	t.false(executor.isReady());

	GroupFactory.destroy(g1);
});

test('test_ready_no_groups', t => {
	const p1 = ProjectFactory.create('TEMP', 'TEST');
	executor.loadProjects();

	t.false(executor.isReady());

	ProjectFactory.destroy(p1);
});

test('test_ready_too_few_places', t => {
	const p1 = ProjectFactory.create('TEMP', 'TEST');
	p1.setPlaces(2);

	const g1 = GroupFactory.create('1');
	const g2 = GroupFactory.create('2');
	const g3 = GroupFactory.create('3');

	t.false(executor.isReady());

	ProjectFactory.destroy(p1);
	GroupFactory.destroy(g1);
	GroupFactory.destroy(g2);
	GroupFactory.destroy(g3);
});

test('test_reverse_lookup_creation', t => {
	executor.loadProjectsArray([
		[0, 2],
		[1, 1]
	]);
	executor._queueCacheRebuild(false);

	t.deepEqual(executor._reverseProjectLookup, [0, 0, 1]);

	executor.loadProjectsArray([
		[0, 3],
		[1, 1],
		[2, 2],
	]);
	executor._queueCacheRebuild(false);

	t.deepEqual(executor._reverseProjectLookup, [0, 0, 0, 1, 2, 2]);
});

test('test_project_matrix_creation', t => {
	executor.loadProjectsArray([
		[0, 2],
		[1, 1]
	]);
	executor.loadGroupsArray([
		['1', [0, 1]],
		['2', [1, 0]]
	]);

	executor._queueCacheRebuild(false);

	t.deepEqual(executor._projectMatrix, [[0, 1], [1, 0]]);
});

test('test_project_matrix_creation_sparse', t => {
	executor.loadProjectsArray([
		[0, 2],
		[1, 1],
		[2, 2]
	]);
	executor.loadGroupsArray([
		['1', [1, 2]],
		['2', [1, 0]],
		['3', [0, 2]],
		['4', [2, 0]]
	]);

	executor._queueCacheRebuild(false);

	t.deepEqual(
		executor._projectMatrix,
		[
			[5, 0, 1],
			[1, 0, 5],
			[0, 5, 1],
			[1, 5, 0]
		]
	);
});

test('test_munkres_matrix_creation', t => {
	executor.loadProjectsArray([
		[0, 2],
		[1, 1]
	]);
	executor.loadGroupsArray([
		['1', [0, 1]],
		['2', [1, 0]]
	]);

	executor._queueCacheRebuild(false);

	t.deepEqual(
		executor._munkresMatrix,
		[
			[0, 0, 1],
			[1, 1, 0]
		]
	);
});

test('test_munkres_matrix_creation_sparse', t => {
	executor.loadProjectsArray([
		[0, 2],
		[1, 1],
		[2, 2]
	]);
	executor.loadGroupsArray([
		['1', [1, 2]],
		['2', [1, 0]],
		['3', [0, 2]],
		['4', [2, 0]]
	]);

	executor._queueCacheRebuild(false);

	t.deepEqual(
		executor._munkresMatrix,
		[
			[5, 5, 0, 1, 1],
			[1, 1, 0, 5, 5],
			[0, 0, 5, 1, 1],
			[1, 1, 5, 0, 0]
		]
	);
});

test.cb('test_compute', t => {
	t.plan(1);

	executor.loadProjectsArray([
		[0, 2],
		[1, 1]
	]);
	executor.loadGroupsArray([
		['1', [0, 1]],
		['2', [1, 0]]
	]);

	executor.compute().then(result => {
		t.deepEqual(result.getRawResult(), [
			[0, 0],
			[1, 2]
		]);
		t.end();
	})
});

test.cb('test_compute_sparse', t => {
	t.plan(1);

	executor.loadProjectsArray([
		[0, 2],
		[1, 1],
		[2, 2]
	]);
	executor.loadGroupsArray([
		['1', [1, 2]],
		['2', [1, 0]],
		['3', [0, 2]],
		['4', [2, 0]]
	]);

	executor.compute().then(result => {
		t.deepEqual(result.getRawResult(), [
			[0, 2],
			[1, 0],
			[2, 1],
			[3, 4]
		]);
		t.end();
	})
});