import test from 'ava';
import * as Util from "../../Util";
import Publicator from "../../Project/Publicator";
import Result from "../../Munkres/Result";
import MunkresExecutor from "../../Munkres/Executor";
import Project from '../../Project/Project';
import Group from "../../Group/Group";
import ProjectFactory from "../../Project/Factory";
import GroupFactory from "../../Group/Factory";

let groups: Group[];
let projects: Project[];
let sparseResult: Result;
let richResult: Result;

test.before(async () => {
	Util.disableLog();
	Publicator.disable();

	let p1 = ProjectFactory.create('Test1', 'Test');
	p1.setPlaces(2);
	let p2 = ProjectFactory.create('Test2', 'Test');
	p1.setPlaces(1);
	let p3 = ProjectFactory.create('Test3', 'Test');
	p3.setPlaces(2);
	projects = [p1, p2, p3];

	let g1 = GroupFactory.create('1');
	g1.setPreferences([p2.getId(), p3.getId()]);
	let g2 = GroupFactory.create('2');
	g2.setPreferences([p2.getId(), p1.getId()]);
	let g3 = GroupFactory.create('3');
	g3.setPreferences([p1.getId(), p3.getId()]);
	let g4 = GroupFactory.create('4');
	g4.setPreferences([p3.getId(), p1.getId()]);
	groups = [g1, g2, g3, g4];

	let executor = new MunkresExecutor();

	executor.loadProjects();
	executor.loadGroups();
	richResult = await executor.compute();
	richResult.apply();

	executor.loadProjectsArray([
		[0, 2],
		[1, 1]
	]);
	executor.loadGroupsArray([
		['1', [0, 1]],
		['2', [1, 0]]
	]);
	sparseResult = await executor.compute();
});

test('confirm_init_state', t => {
	t.deepEqual(richResult.getRawResult(), [[0, 2], [1, 1], [2, 0], [3, 3]]);
	t.deepEqual(sparseResult.getRawResult(), [[0, 0], [1, 2]]);
});

test('test_assignment_stats', t => {
	t.deepEqual(richResult.getAssignmentStats(), {
		'0': 3,
		'1': 1
	});
	t.deepEqual(sparseResult.getAssignmentStats(), {
		'0': 2
	});
});

test('apply', t => {
	t.true(groups[0].getAssignment() == projects[2]);
	t.true(groups[1].getAssignment() == projects[1]);
	t.true(groups[2].getAssignment() == projects[0]);
	t.true(groups[3].getAssignment() == projects[2]);
});