import test from 'ava';
import {comparePassword, hashPassword} from "../PasswordUtil";

const password = 'Test_Password!';
const expectedHash = '$2b$12$zzJkFCyFw0uRbMUpyMs6fu/mMDtWWpu1.a3Diswy7EipbRX5l/aM2';
const defWrong = '$2b$12$rRkgtqVx2aL8pfmKMwq8VO.NQ24EldpGpbB0Pv0B6eVCFwDoldg8Q';

test.cb('test_hashing', t => {
	t.plan(2);
	hashPassword(password).then(hash => {
		t.is(hash.length, 60, 'expect a hash of 60 chars');
		comparePassword(password, hash).then(isMatch => {
			t.true(isMatch, 'hash matches password');
			t.end();
		});
	})
});

test.cb('test_hashing_not_string', t => {
	t.plan(1);
	hashPassword(null).catch(() => {
		t.pass('hashing a non-string should yield an error');
		t.end();
	})
});

test.cb('compare_correct', t => {
	t.plan(1);
	comparePassword(password, expectedHash).then(isMatch => {
		t.true(isMatch, 'Password should be correct');
		t.end();
	});
});

test.cb('compare_wrong', t => {
	t.plan(1);
	comparePassword(password, defWrong).then(isMatch => {
		t.false(isMatch, 'hash shouldn\'t match password');
		t.end();
	})
});

test.cb('compare_null', t => {
	t.plan(1);
	comparePassword(null, defWrong).catch(() => {
		t.pass('comparing with null should yield an error');
		t.end();
	})
});