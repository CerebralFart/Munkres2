import ProjectFactory from "../../Project/Factory";
import test from "ava";
import Publicator from "../../Project/Publicator";

test.before(t => {
	Publicator.disable();
});

test.cb('test_creation_publication_on_ws', t => {
	t.plan(1);

	Publicator._updateCallback = () => {
		t.pass();
		t.end();
	};

	ProjectFactory.create('CREATION_PUBLICATION_TEST', '');
});

test.cb('test_update_publication_on_ws', t => {
	t.plan(1);

	let project = ProjectFactory.create('UPDATE_PUBLICATION_TEST', '');

	Publicator._updateCallback = () => {
		t.pass();
		t.end();
	};

	project.setDescription('A');
});

test('', t => {
	Publicator.enable();
	t.throws(() => {
		ProjectFactory.create('UPDATE_PUBLICATION_ENABLED_TEST', '');
	});
});