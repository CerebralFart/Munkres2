import * as moment from 'moment';
import Config from './Config';

export const log = (data: (string | number)[] | string) => {
	if (!Config.silent) {
		if (Array.isArray(data)) {
			data = data.join(' : ');
		}
		console.log('[' + moment().format('HH:mm:ss.SSS') + '] ' + data)
	}
};

export const disableLog = () => {
	Config.silent = true;
};
