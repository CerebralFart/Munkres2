import {Server} from 'ws';
import {log} from './Util';
import StatusObject from "./StatusObject";
import ProjectFactory from "./Project/Factory";
import {comparePassword, hashPassword} from "./PasswordUtil";
import Config from "./Config";
import GroupFactory from "./Group/Factory";
import MunkresExecutor from "./Munkres/Executor";
import Project from "./Project/Project";
import Group from "./Group/Group";

let authCodeLookup: { [key: string]: string } = {};
let executor;

const commands: { [key: string]: Function } = {
	'assignment': (message, auth) => {
		const assignment = GroupFactory.getByCode(authCodeLookup[auth]).getAssignment();
		if (assignment === null) {
			return null;
		} else {
			return assignment.getId();
		}
	},
	'authorization': (message, auth, ws) => {
		return new Promise((resolve, reject) => {
			const pseudoResolve = (code, type, i?: number) => {
				if (i === undefined) i = 0;
				hashPassword(code).then(hash => {
					if (Object.keys(authCodeLookup).indexOf(hash) === -1) {
						authCodeLookup[hash] = code;
						resolve({hash, type})
					} else {
						pseudoResolve(code, hash, i + 1);
					}

					resolve({hash, type})
				}).catch(e => {
					reject(e);
				});
			};

			if (message.code === Config.adminCode) {
				pseudoResolve(message.code, 'admin');
			} else {
				try {
					const group = GroupFactory.getByCode(message.code);
					group.addWs(ws);
					pseudoResolve(message.code, 'user');
				} catch (e) {
					resolve({error: 'incorrect'});
				}
			}
		});
	},
	'ping': () => 'pong',
	'preferences': (message, auth) => GroupFactory.getByCode(authCodeLookup[auth]).getPreferences(),
	'set-preferences': (message, auth, ws) => GroupFactory.getByCode(authCodeLookup[auth]).setPreferences(message),
	'projects': () => ProjectFactory.getInstances(),
	'status': () => StatusObject,
	'admin-groups': () => GroupFactory
		.getInstances()
		.map((group: Group) => {
			return {
				id: group.getId(),
				code: group.getCode(),
				preferences: group.getPreferences(),
			};
		}),
	'admin-groups-set': (message) => {
		const count = parseInt(message.count, 10);
		const initLength = GroupFactory.getInstances().length;
		if (count > initLength) {
			while (count !== GroupFactory.getInstances().length) {
				GroupFactory.createNoCode()
			}
		} else if (count < initLength) {
			while (count !== GroupFactory.getInstances().length) {
				const group = GroupFactory.getInstances().slice(-1)[0];
				GroupFactory.destroy(group);
			}
		}
		return {};
	},
	'admin-add-project': (message) => {
		let project: Project;
		let error = "Something went wrong";
		try {
			project = ProjectFactory.create(message.name, message.by);
			project.setDescription(message.description);
			project.setPlaces(message.places);
			if (project.isValid())
				return {status: 'okay'}
		} catch (e) {
			error = e.getMessage();
		}
		if (project !== undefined)
			ProjectFactory.destroy(project);
		return {status: 'invalid', error}
	},
	'admin-edit-project': (message) => {
		let project = ProjectFactory.getById(message.id);
		project.setRollbackPoint();
		try {
			if (message.name !== project.getName())
				project.setName(message.name);
			if (message.by !== project.getBy())
				project.setBy(message.by);
			else if (message.by === "")
				project.setBy(project.getName());
			if (message.places !== project.getPlaces())
				project.setPlaces(message.places);
			if (message.description !== project.getDescription())
				project.setDescription(message.description);
			if (project.isValid()) {
				return {status: 'okay'}
			}
		} catch (e) {
			console.log(e);
		}
		project.revert();
		return {status: 'invalid'}
	},
	'admin-delete-project': (message) => {
		let project = ProjectFactory.getById(message.id);
		if (project === null) return {};
		ProjectFactory.destroy(project);
		return {};
	},
	'admin-open-voting': () => {
		if (ProjectFactory.getInstances().length === 0) {
			return {
				status: 'error',
				error: 'No projects are configured'
			}
		}
		executor.loadProjects();

		if (GroupFactory.getInstances().length === 0) {
			return {
				status: 'error',
				error: 'No groups are configured'
			}
		}
		executor.loadGroups();

		if (!executor.isReady()) {
			return {
				status: 'error',
				error: 'There are not enough places to assign all groups'
			};
		}

		StatusObject.status = 'voting';
		StatusObject.closeTime = Date.now() + StatusObject.votingDuration;
		const check = () => {
			if (Date.now() > StatusObject.closeTime) {
				executor.loadGroups();
				StatusObject.status = 'computing';
				executor.compute().then(result => {
					result.apply();
					StatusObject.status = 'done';
					StatusObject.assignments = result.getByProject();
					StatusObject.statistics = result.getAssignmentStats();
				});
			} else {
				setTimeout(check, 5000);
			}
		};
		check();
		return {
			status: 'success'
		}
	},
	'admin-export': () => {
		let projectMapper = project => {
			return project === null ? null : project.getName();
		};

		return {
			projects: ProjectFactory.getInstances().map(project => {
				return {
					name: project.getName(),
					by: project.getBy(),
					places: project.getPlaces(),
					description: project.getDescription()
				}
			}),
			groups: GroupFactory.getInstances().map(group => {
				return {
					code: group.getCode(),
					preferences: group.getPreferences().map(project => projectMapper(ProjectFactory.getById(project))),
					assignment: projectMapper(group.getAssignment())
				}
			}),
			status: {
				title: StatusObject.title
			}
		}
	},
	'admin-import': message => {
		if (StatusObject.status !== 'initializing') return {status: 'error'};

		ProjectFactory.clear();
		GroupFactory.clear();

		let projectNameLookup: { [key: string]: Project } = {};

		message.projects.forEach(project => {
			let instance = ProjectFactory.create(project.name, project.by);
			instance.setPlaces(project.places);
			instance.setDescription(project.description);
			projectNameLookup[project.name] = instance;
		});
		message.groups.forEach(group => {
			let instance = GroupFactory.create(group.code);

			let assignedProject = projectNameLookup[group.assignment];
			if (assignedProject !== undefined) {
				instance.setAssignment(assignedProject);
			}

			let preferences = [];
			group.preferences.forEach(pref => {
				let project = projectNameLookup[pref];
				if (project !== undefined) {
					preferences.push(project.getId());
				}
			});
			instance.setPreferences(preferences)
		});
		StatusObject.title = message.status.title;

		return {status: 'okay'};
	},
	'admin-set': message => {
		let protectedProps = ['status'];
		if (message.key in StatusObject && protectedProps.indexOf(message.key) === -1) {
			StatusObject[message.key] = message.value;
			return {status: 'okay'};
		} else {
			return {status: 'error'};
		}
	}
};

class WSServer {
	wss: Server;

	init(server) {
		executor = new MunkresExecutor();
		this.wss = new Server({server: server});
		log(['websocket', 'started']);
		this.wss.on('connection', (ws) => {
			ws.on('message', message => WSServer.onMessage(message, ws));
		});
	}

	private static isValidAuth(authCode, ws) {
		const code = authCodeLookup[authCode];
		const group = GroupFactory.getByCode(code);
		return group.ws.indexOf(ws) !== -1;
	}

	private static async isValidAdminAuth(authCode) {
		return await comparePassword(Config.adminCode, authCode);
	}

	private static async onMessage(message, ws) {
		log(['websocket', 'recieved', message]);
		try {
			const data = JSON.parse(message);
			const command = data.command;
			if (Object.keys(commands).indexOf(command) !== -1) {
				if (command.startsWith('admin-')) {
					if (data.auth === undefined) return;
					const isAdmin = await WSServer.isValidAdminAuth(data.auth);
					if (!isAdmin) return;
				} else if (command.startsWith('set-')) {
					if (data.auth === undefined) return;
					if (!this.isValidAuth(data.auth, ws)) return;
				}
				let executor = commands[command](data.params, data.auth, ws);
				if (!(executor instanceof Promise)) {
					executor = new Promise(resolve => resolve(executor));
				}
				executor.then(response => {
					if (response === undefined) return;
					WSUtil.send(ws, {
						command,
						response
					});
				});
				executor.catch(response => {
					WSUtil.send(ws, {
						command,
						response
					});
				})
			} else {
				WSUtil.send(ws, {
					command: 'error',
					error: 'command not found',
					params: {
						command: data.command
					}
				});
			}
		} catch (e) {
			WSUtil.send(ws, {
				command: 'error',
				error: 'message invalid'
			});
		}
	}

	broadcast(command, data) {
		this.wss.clients.forEach(client => WSUtil.send(client, {
			command,
			response: data
		}));
	}
}

export default new WSServer();

let WSUtilCallback = () => true;

export const WSUtil = {
	send: (ws, data) => {
		if (!WSUtilCallback()) return;
		if (ws.readyState !== ws.OPEN) return;
		if (data === undefined) return;
		if (typeof data === 'string') {
			data = {
				response: data
			};
		}
		ws.send(JSON.stringify(data));
	},
	setCallback: callback => {
		WSUtilCallback = callback;
	}
};
