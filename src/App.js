import React from 'react';
import styled from 'styled-components';
import {Redirect, Route, Switch} from "react-router-dom";
import Dashboard from "./Pages/Dashboard";
import Statistics from "./Pages/Statistics";
import Assignments from "./Pages/Assignments";
import Preferences from "./Pages/Preferences";
import ProjectOverview from './Pages/Project/Overview';
import ProjectAdminOverview from './Pages/Project/AdminOverview';
import ProjectDetails from './Pages/Project/Details';
import ProjectEdit from './Pages/Project/Edit';
import ProjectNew from './Pages/Project/New';
import GroupAdminOverview from './Pages/Group/AdminOverview'
import {CombinedContextProvider} from "./CombinedContext";
import AuthWindow from "./AuthWindow";
import Socket from './Socket';
import {Header, SideNav} from "./Menu/";
import {updateUserContext, UserContextConsumer} from "./UserContext";
import {StatusContextConsumer} from "./StatusContext";
import ConfigOverview from "./Pages/Config/Overview";
import ConfigSave from "./Pages/Config/Save";
import ConfigLoad from "./Pages/Config/Load";
import {NotificationDisplay} from "./Notification";

const ContentWrapper = styled.div`
	padding: 45px 5% 3em;
	
	& h1 {
		text-align: center;
		padding-top: 3em;
		padding-bottom: 1em;
		font-size: 2rem;
	}
	
	@media only screen and (min-width: 768px) {
		margin-left: 110px;
		padding-top: 55px;
		& h1 {
			font-size: 3.2rem;
			font-weight: 300;
		}
	}
	@media only screen and (min-width: 1170px) {
		margin-left: 200px;
	}
`;

class App extends React.Component {
	state = {
		menuIsOpen: false,
		groups: [],
		assignments: []
	};

	constructor(props) {
		super(props);

		this.openMenu = this.openMenu.bind(this);
		this.closeMenu = this.closeMenu.bind(this);
	}

	openMenu() {
		this.setState({menuIsOpen: true});
	}

	closeMenu() {
		this.setState({menuIsOpen: false})
	}

	getRoutes(user, status) {
		let routes = [];
		if (user.isAdmin) {
			routes.push(<Route key='ag' path="/admin/groups" exact component={GroupAdminOverview}/>);
			routes.push(<Route key='ap' path="/admin/projects" exact component={ProjectAdminOverview}/>);
			routes.push(<Route key='apn' path="/admin/projects/new" exact component={ProjectNew}/>);
			routes.push(<Route key='api' path="/admin/projects/:id" exact component={ProjectEdit}/>);
			routes.push(<Route key='ac' path='/admin/config' exact component={ConfigOverview}/>);
			routes.push(<Route key='acs' path='/admin/config/save' exact component={ConfigSave}/>);
			routes.push(<Route key='acl' path='/admin/config/load' exact component={ConfigLoad}/>);
		} else {
			routes.push(<Route key='prf' path="/preferences" exact component={Preferences}/>);
		}

		if (status.status === 'done') {
			routes.push(<Route key='rst' path="/statistics" exact component={Statistics}/>);
			routes.push(<Route key='ras' path="/assignments" exact component={Assignments}/>);
		}


		routes.push(<Route key='pro' path="/projects" exact component={ProjectOverview}/>);
		routes.push(<Route key='pri' path="/projects/:id" render={({match}) => <ProjectDetails projectId={match.params.id}/>}/>);
		routes.push(<Route key='das' path="/" exact component={Dashboard}/>);
		routes.push(<Redirect key='dir' to="/"/>);

		return routes;
	}

	render() {
		if (Socket.hasClientSecret()) {
			return (
				<CombinedContextProvider>
					<Header
						isOpen={this.state.menuIsOpen}
						open={this.openMenu}
						close={this.closeMenu}
					/>

					<main className="cd-main-content">
						<SideNav isOpen={this.state.menuIsOpen} close={this.closeMenu}/>
						<ContentWrapper>
							<UserContextConsumer>
								{user =>
									<StatusContextConsumer>
										{status =>
											<Switch>
												{this.getRoutes(user, status)}
											</Switch>
										}
									</StatusContextConsumer>
								}
							</UserContextConsumer>
						</ContentWrapper>
					</main>
					<NotificationDisplay/>
				</CombinedContextProvider>
			);
		} else {
			//TODO make size dynamically loaded
			return <AuthWindow
				validator={value => {
					return new Promise((resolve, reject) => {
						Socket.registerSingleMessageHandler('authorization', data => {
							if (data.response.error) {
								reject();
							} else {
								Socket.setClientSecret(data.response.hash);
								updateUserContext(data.response.type);
								this.forceUpdate();
								resolve();
							}
						});
						Socket.sendCommand('authorization', {code: value});
					});
				}}
				size={4}
			/>
		}
	}
}

export default App;
