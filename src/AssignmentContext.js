import React from 'react';
import Socket from './Socket';
import {registerContext} from "./CombinedContext";
import {isAdmin} from "./UserContext";

export const assignmentSymbol = Symbol('assignment');
const AssignmentContext = React.createContext(assignmentSymbol);

export class AssignmentContextProvider extends React.Component {
	state = {
		id: null
	};

	constructor(props) {
		super(props);
		Socket.registerMessageHandler('assignment', message => {
			this.setState({id: message.response});
		});
		Socket.sendCommand('assignment');
	}

	render() {
		return (
			<AssignmentContext.Provider value={this.state.id}>
				{this.props.children}
			</AssignmentContext.Provider>
		)
	}
}

export const AssignmentContextConsumer = AssignmentContext.Consumer;

registerContext(assignmentSymbol, AssignmentContextConsumer, AssignmentContextProvider, () => !isAdmin());