import React from 'react';
import styled, {css, keyframes} from 'styled-components';
import ProcessingOverlay from "./ProcessingOverlay";

const Container = styled.div`
	position: relative;
	width: 250pt;
	height: 150pt;
	margin: 150pt auto;
	background-color: antiquewhite;
	border-radius: 5pt;
	transition: 0.25s;
	
	${props => props.error && css`
		animation: ${shake} 1s;
		background-color: lightcoral;
	`}
`;

const shake = keyframes`
	10%, 90% {
		transform: translate3d(-1px, 0, 0);
	}
	
	20%, 80% {
		transform: translate3d(3px, 0, 0);
	}
	
	30%, 50%, 70% {
		transform: translate3d(-4px, 0, 0);
	}
	
	40%, 60% {
		transform: translate3d(4px, 0, 0);
	}
`;

const Text = styled.div`
	position: absolute;
	top: 30pt;
	font-size: 20pt;
	text-align: center;
`;

const Inputs = styled.div`
	position: absolute;
	top: 90pt;
	width: 100%;
	text-align: center;
`;

const Input = styled.input`
	width: 50px;
	height: 60px;
	line-height: 100%;
	background-color: transparent;
	border: 0;
	outline: 0;
	color: white;
	font-size: 60px;
	word-spacing: 0px;
	overflow: hidden;
	text-align: center;
	
	-moz-appearance: textfield;
	
	&::-webkit-outer-spin-button,
	&::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
	}
`;

class AuthWindow extends React.Component {
	state = {
		value: '',
		processing: false,
		pwdCorrect: null
	};
	mounted = true;

	constructor(props) {
		super(props);

		this._onClick = this._onClick.bind(this);
		this._onKey = this._onKey.bind(this);
		this._clearPwd = this._clearPwd.bind(this);
	}

	componentWillUnmount() {
		this.mounted = false;
	}

	_onClick() {
		let focusable = document.getElementById('lastInput');
		if (focusable === null) return;
		let done = false;
		while (!done) {
			const previous = focusable.previousSibling;
			if (previous === null || previous.value !== '') {
				done = true;
			} else {
				focusable = previous;
			}
		}
		focusable.focus();
	}

	_onKey(evt) {
		let key = evt.keyCode;
		let value = this.state.value;
		if (key === 8) {
			value = value.substring(0, value.length - 1);
		} else {
			if (96 <= key && key <= 105) key -= 48;
			const char = String.fromCharCode(key).toUpperCase();
			if (!/^[0-9A-Z]$/.test(char)) return;
			value += char;
			value = value.substring(0, this.props.size);
		}
		this.setState({value});
		if (value.length === this.props.size) {
			let validator = this.props.validator(value);
			if (validator.then !== undefined && typeof validator.then === 'function') {
				//It is a promise, handle that
				this.setState({processing: true});
				validator
					.then(() => this.setState({
						pwdCorrect: true
					}))
					.catch(() => this.setState({
						pwdCorrect: false
					}))
					.finally(() => this.setState({
						processing: false
					}))
			} else if (typeof validator === 'boolean') {
				// Results are in!
				this.setState({
					pwdCorrect: validator
				})
			}

		}
	}

	_clearPwd() {
		this.setState({pwdCorrect: null})
	}

	setState(props) {
		if (this.mounted) {
			super.setState(props);
		}
	}

	render() {
		const value = this.state.value;
		if (this.state.pwdCorrect !== null) {
			window.setTimeout(this._clearPwd, 1100);
			document.activeElement.blur();
		} else {
			window.setTimeout(this._onClick);
		}
		return <Container error={this.state.pwdCorrect === false}>
			<Text>Please enter the authentication code.</Text>
			<Inputs id='inputs' onClick={this._onClick} onChange={this._onClick} onKeyDown={this._onKey}>
				{Array(this.props.size - 1).fill('').map((_, i) =>
					<Input key={i} maxLength="2" placeholder="•" value={value[i] || ''} type='number'/>
				)}
				<Input id='lastInput' maxLength="1" placeholder="•" value={value[this.props.size - 1] || ''}
					   type='number'/>
			</Inputs>
			{this.state.processing && <ProcessingOverlay/>}
		</Container>
	}
}

export default AuthWindow;