import React, {Component} from 'react';

class Clock extends Component {
	interval = null;
	state = {
		minutes: 0,
		seconds: 0
	};

	componentWillMount() {
		this.updateTimeLeft(this.props.deadline);
	}

	componentDidMount() {
		this.interval = setInterval(() => this.updateTimeLeft(this.props.deadline), 1000);
	}

	componentWillUnmount() {
		this.clearInterval();
	}

	clearInterval(){
		if (this.interval !== null) {
			clearInterval(this.interval);
			this.interval = null;
		}
	}

	leading0(num) {
		return num < 10 ? '0' + num : num;
	}

	updateTimeLeft(deadline) {
		const time = deadline - Date.now();
		if (time <= 0) {
			this.setState({minutes: 0, seconds: 0});
			this.clearInterval();
		} else {
			const seconds = Math.floor((time / 1000) % 60);
			const minutes = Math.floor((time / 60000) % 60);
			this.setState({minutes, seconds});
		}
	}

	render() {
		return <span>{this.leading0(this.state.minutes)}:{this.leading0(this.state.seconds)}</span>;
	}
}

export default Clock;