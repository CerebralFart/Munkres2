import React from 'react';
import {Function, SafeComponent} from "./Util";
import isEqual from "lodash.isequal";

let contextLookup = {};
let contexts = [];
export const registerContext = (symbol, consumer, provider, condition) => {
	condition = condition || (() => true);
	let value = () => (condition() && [consumer, provider]);
	contextLookup[symbol] = value;
	contexts.push(value);
};

const extract = (contexts, provider) =>
	contexts
		.map(f => f())
		.filter(e => e !== false)
		.map(c => c[provider ? 1 : 0]);

export const CombinedContextProvider = ({children}) => {
	return extract(contexts, true).reduceRight(
		(accumulated, Provider) => <Provider>{accumulated}</Provider>,
		children
	);
};


export class CombinedContextConsumer extends SafeComponent {
	state = {
		values: []
	};
	consumers = [];

	constructor(props) {
		super(props);
		let {contexts} = this.props;
		this.state.values = new Array(contexts.length);
		this.consumers = extract(contexts.map(symbol => contextLookup[symbol]), false);
	}

	render() {
		let {children} = this.props;
		return this.consumers.reduce((acc, Consumer, idx) => {
			return <Consumer>
				{value => {
					let values = this.state.values;
					if (!isEqual(values[idx], value)) {
						values[idx] = value;
						setTimeout(() => this.setState({values}));
					}
					return acc;
				}}
			</Consumer>
		}, <Function>
			{() => children(...this.state.values)}
		</Function>);
	}
}