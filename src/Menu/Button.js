import React from 'react';
import styled, {css} from "styled-components";

export const LiWrapper = styled.li`
	@media only screen and (min-width: 1170px) {
		text-align: left;
	}
`;

export const AWrapper = styled.a`
	display: block;
	margin: 0 5%;
	padding: 1em 0;
	background-color: #1784c7;
	border-radius: .25em;
	border: none;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3), inset 0 1px 0 rgba(255, 255, 255, 0.2);
	text-align: center;
	color: #ffffff;
	font-weight: bold;
	
	&::before {
		display:none;
	}
	
	@media only screen and (min-width: 768px) {
		margin: 1em 10% 0;
	}
	
	@media only screen and (min-width: 1170px) {
		margin: 0 18px;

		&:hover {
			background-color: #1a93de;
		}
	}
	
	${props => props.onClick !== undefined && css`
		cursor: pointer
	`}
`;

const MenuButton = ({children, liElem, aElem, disabled, status, onClick}) => {
	const LiElem = liElem || LiWrapper;
	const AElem = aElem || AWrapper;

	return (
		<LiElem>
			<AElem onClick={disabled ? undefined : onClick}>
				{children}
			</AElem>
		</LiElem>
	);
};

export default MenuButton;