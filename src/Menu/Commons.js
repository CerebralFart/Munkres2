import styled, {css} from "styled-components";
import {Link} from "react-router-dom";

export const StyledLink = styled(Link)`
	${props => props.active === 1 && css`
		box-shadow: inset 3px 0 0 #1784c7;
		background-color: #33383e;
	`}
`;