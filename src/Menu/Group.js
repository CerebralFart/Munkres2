import React from 'react';
import {withRouter} from "react-router-dom";
import styled, {css} from "styled-components";
import {StyledLink} from "./Commons";

const Count = styled.span`
	position: absolute;
	width: auto;
	height: auto;
	top: 50%;
	right: 18px;
	transform: translateY(-50%);
	padding: 0.2em 0.4em;
	background-color: #ff7e66;
	border-radius: .25em;
	color: #ffffff;
	font-weight: bold;
	font-size: 1.2rem;
	text-align: center;
	box-shadow: none;
	
	@media only screen and (max-width: 1170px) {
		display:none;
	}
`;
const StyledLi = styled.li`
	@media only screen and (min-width: 1170px) {
		${props => props.active && css`
			& ul {
				position: relative;
				display: block;
				left: 0;
				box-shadow: none;
			}
		`}
	}
`;

const Group = ({children, name, path, location, hasCount, close}) => {
	const active = location.pathname.startsWith(path);
	return <StyledLi className={'has-children'} active={active}>
		<StyledLink to={path} onClick={close} active={location.pathname.startsWith(path) ? 1 : 0}>
			{name}
			{hasCount && <Count>{children.length}</Count>}
		</StyledLink>
		<ul>
			{children}
		</ul>
	</StyledLi>;
};

export default withRouter(Group);