import React from 'react';
import styled, {css} from "styled-components";

const Container = styled.a`
	float: right;
	position: relative;
	display: block;
	width: 34px;
	height: 44px;
	margin-right: 5%;
	overflow: hidden;
	white-space: nowrap;
	color: transparent;
	
	@media only screen and (min-width: 768px) {
		display: none;
	}
`;

const Base = styled.span`
	position: absolute;
	right: 5px;
	height: 3px;
	width: 24px;
	display: inline-block;
	background: #ffffff;
	
	-webkit-transform: translateZ(0);
	-moz-transform: translateZ(0);
	-ms-transform: translateZ(0);
	-o-transform: translateZ(0);
	transform: translateZ(0);
	
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
	
	-webkit-transform-origin: 0% 50%;
	-moz-transform-origin: 0% 50%;
	-ms-transform-origin: 0% 50%;
	-o-transform-origin: 0% 50%;
	transform-origin: 0% 50%;		
`;

const TopBar = Base.extend`
	top: calc(50% - 8px);
	
	${props => props.isOpen && css`
		transform: translateX(4px) translateY(-3px) rotate(45deg);
	`}
`;

const MiddleBar = Base.extend`
	top: calc(50% -  2px);
	
	${props => props.isOpen && css`
		display: none;
	`}
`;

const LowBar = Base.extend`
	top: calc(50% + 4px);
	
	${props => props.isOpen && css`
		transform: translateX(4px) translateY(2px) rotate(-45deg);
	`}
`;

const Hamburger = ({isOpen, open, close}) => {
	const onClick = isOpen ? close : open;
	return <Container onClick={onClick}>
		<TopBar isOpen={isOpen}/>
		<MiddleBar isOpen={isOpen}/>
		<LowBar isOpen={isOpen}/>
	</Container>
};

export default Hamburger;