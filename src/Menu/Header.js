import React from 'react';
import styled from "styled-components";
import Hamburger from "./Hamburger";
import {Link} from "react-router-dom";
import {StatusContextConsumer} from "../StatusContext";

const Logo = styled(Link)`
	float: left;
	display: block;
	margin: 11px 0 0 5%;
	
	@media only screen and (min-width: 768px) {
		margin: 16px 0 0 36px;
	}
`;

const HeaderElem = styled.header`
	position: absolute;
	z-index: 2;
	top: 0;
	left: 0;
	height: 45px;
	width: 100%;
	background: #3e454c;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: greyscale;

	&::after {
		clear: both;
		content: "";
		display: table;
	}
	
	@media only screen and (min-width: 768px) {
		position: fixed;
		height: 55px;
	}
`;

const Header = ({isOpen, open, close}) => {
	return <HeaderElem>
		<Logo to="/">
			<StatusContextConsumer>
				{status => status.title ? status.title : "Munkres2"}
			</StatusContextConsumer>
		</Logo>
		<Hamburger
			isOpen={isOpen}
			open={open}
			close={close}
		/>
	</HeaderElem>
};

export default Header;