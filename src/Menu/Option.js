import React from 'react';
import {withRouter} from "react-router-dom";
import {StyledLink} from "./Commons";

const Option = ({to, children, location, close}) => {
	const path = location.pathname;
	return (
		<li>
			<StyledLink to={to} onClick={close} active={path === to ? 1 : 0}>{children}</StyledLink>
		</li>
	);
};

export default withRouter(Option);