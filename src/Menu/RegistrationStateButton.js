import React from 'react';
import Button from "./Button";
import {UserContextConsumer} from "../UserContext";
import {StatusContextConsumer} from "../StatusContext";
import Clock from "../Clock";
import Socket from "../Socket";
import {notify} from "../Notification";

Socket.registerMessageHandler('admin-open-voting', message => {
	let response = message.response;
	if (response.status === 'error') {
		notify(response.error, 'warning');
	} else {
		notify('Voting has opened', 'success');
	}
});

class RegistrationStateButton extends React.Component {
	render() {
		return (
			<StatusContextConsumer>
				{status => {
					if (status.status === 'initializing') {
						return <UserContextConsumer>
							{user => user.isAdmin ? (
								<Button
									onClick={() => Socket.sendCommand('admin-open-voting')}
								>
									Open voting
								</Button>
							) : (
								<Button>Please wait</Button>
							)}
						</UserContextConsumer>
					} else if (status.status === 'voting') {
						return <Button>
							Voting open <Clock deadline={status.closeTime}/>
						</Button>
					} else if (status.status === 'computing') {
						return <Button>Computing</Button>
					} else if (status.status === 'done') {
						return <Button>Done</Button>
					} else {
						return <Button>State unknown</Button>
					}
				}}
			</StatusContextConsumer>

		);
	}
}

export default RegistrationStateButton;