import React from 'react';
import SocketStateIndicator from "./SocketStateIndicator";
import RegistrationStateButton from "./RegistrationStateButton";
import {ProjectContextConsumer} from "../Project/Context";
import Option from "./Option";
import Group from "./Group";
import styled from "styled-components";
import {UserContextConsumer} from "../UserContext";
import {StatusContextConsumer} from "../StatusContext";

const Label = styled.li`
	display: block;
	padding: 1em 5%;

	text-transform: uppercase;
	font-weight: bold;
	color: #646a6f;
	font-size: 1rem;
	letter-spacing: .1em;
	
	@media only screen and (min-width: 768px) {
		display: none;
	}

	@media only screen and (min-width: 1170px) {
		display: block;
		padding: 1em 18px;
	}
`;

class SideNav extends React.Component {
	constructor(props) {
		super(props);

		this.close = props.close;
		this.withClose = this.withClose.bind(this);
	}

	withClose(Elem) {
		return ({children, ...props}) => <Elem
			close={this.close}
			{...props}
		>
			{children}
		</Elem>
	}

	render() {
		const MenuOption = this.withClose(Option);
		const MenuGroup = this.withClose(Group);
		return <nav className={"cd-side-nav" + (this.props.isOpen ? ' nav-is-visible' : '')}>
			<ul>
				<MenuOption to='/'>Overview</MenuOption>
				<UserContextConsumer>
					{user => user.isAdmin || <MenuOption to='/preferences'>Preferences</MenuOption>}
				</UserContextConsumer>
				<ProjectContextConsumer>
					{projects =>
						<MenuGroup
							name={'Projects'}
							hasCount={7}
							path='/projects'
						>
							{projects.map(project =>
								<MenuOption
									to={'/projects/' + project.id}
									key={project.id}
								>
									{project.name}
								</MenuOption>
							)}
						</MenuGroup>
					}
				</ProjectContextConsumer>

				<StatusContextConsumer>
					{status => status.status === 'done' && <MenuOption to='/assignments'>Assignments</MenuOption>}
				</StatusContextConsumer>
			</ul>

			<UserContextConsumer>
				{user => user.isAdmin &&
					<ul>
						<Label>Administration</Label>
						<MenuOption to='/admin/groups'>
							Groups
						</MenuOption>
						<MenuGroup
							path='/admin/projects'
							name='Projects'
						>
							<MenuOption to='/admin/projects/new'>New Project</MenuOption>
						</MenuGroup>
						<MenuGroup
							path='/admin/config'
							name='Configuration'
						>
							<MenuOption to='/admin/config/save'>Save Configuration</MenuOption>
							<MenuOption to='/admin/config/load'>Load Configuration</MenuOption>
						</MenuGroup>
					</ul>
				}
			</UserContextConsumer>

			<ul>
				<Label>Status</Label>
				<RegistrationStateButton/>
				<SocketStateIndicator/>
			</ul>
		</nav>

	}
}

export default SideNav;