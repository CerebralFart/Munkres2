import React from 'react';
import {SocketStatusConsumer} from "../Socket";
import Button, {LiWrapper} from "./Button";

class SocketStateIndicator extends React.Component {
	render() {
		const liElem = LiWrapper.extend`
			@media only screen and (min-width: 1170px) {
				position: absolute;
				bottom: 1em;
				width: 100%;
			}
		`;

		return (
			<SocketStatusConsumer>
				{status =>
					<Button
						liElem={liElem}
						disabled={status}
						onClick={window.location.reload.bind(window.location)}
					>
						{status ? 'Connection OK' : 'Connection failed'}
						{status || <i style={{marginLeft: '5px'}} className='fas fa-sync'/>}
					</Button>
				}
			</SocketStatusConsumer>
		);
	}
}

export default SocketStateIndicator;