import React from 'react';
import styled from 'styled-components';
import {shadeColor} from "./Util";

const closeSpeed = 375;

const NotificationContainer = styled.div`
	position: fixed;
	bottom: 15pt;
	right: 15pt;
`;

const Notification = styled.div`
	width: 150pt;
	height: ${props => props.hiding ? '0pt' : '75pt'};
	transition: ${closeSpeed / 1000 + 's'} linear;
	
	padding: 6pt;
	margin-top: 10pt;
	border-radius: 6pt;
	border: solid 2pt ${props => shadeColor(props.color, -0.25)};
	background-color: ${props => props.color};
	color: white;
	overflow: hidden;
`;

const CloseButton = (() => {
	const CloseWrapper = styled.div`
		float: right;
		cursor: pointer;
	`;

	return ({id}) => {
		return <CloseWrapper onClick={() => closeNotification(id)}>×</CloseWrapper>
	};
})();

let notifications = [];
const closeNotification = (id) => {
	const notif = notifications.filter(not => not.id === id)[0];
	if (notif === undefined) return;
	notif.hiding = true;
	setTimeout(() => {
		let index = notifications.indexOf(notif);
		notifications.splice(index, 1);
		notifyListeners();
	}, closeSpeed)
};

let listeners = [];
const notifyListeners = () => {
	for (let listener of listeners) {
		listener();
	}
};

const colors = {
	error: '#dc3545',
	warning: '#ffc107',
	success: '#28a745',
	info: '#17a2b8'
};

export class NotificationDisplay extends React.Component {
	state = {
		notifications: []
	};

	listening = true;

	componentDidMount() {
		listeners.push(() => {
			if (this.listening) {
				this.setState({notifications})
			}
		});
	}

	componentWillUnmount() {
		this.listening = false;
	}

	render() {
		let notifs = notifications.sort((a, b) => a.id - b.id);
		return <NotificationContainer>
			{notifs.reverse().map(notification =>
				<Notification key={notification.id} color={colors[notification.type]}
							  hiding={notification.hiding || false}>
					<CloseButton id={notification.id}/>
					{notification.text}
				</Notification>
			)}
		</NotificationContainer>
	}
}

let counter = 0;

export const notify = (text, type, action) => {
	type = type || 'warning';
	let id = counter;
	notifications.push(new Proxy({
		id,
		text,
		type,
		action,
		hiding: false
	}, {
		set: (target, prop, value) => {
			target[prop] = value;
			notifyListeners();
			return true;
		}
	}));
	setTimeout(() => closeNotification(id), 10000);
	notifyListeners();
	counter++;
};