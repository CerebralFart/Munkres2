import React from 'react';
import styled from 'styled-components';
import {ordinalSuffix, Wrapper} from "../Util";
import {ProjectContextConsumer} from "../Project/Context";
import Config from "../Config";
import {StatusContextConsumer} from "../StatusContext";
import {Link} from "react-router-dom";

const Sup = styled.sup`
	font-size: 0.75em;
	vertical-align: top;
`;

const Table = styled.table`
	& td:first-child {
		padding-right: 10pt;
	}
`;

const Hr = styled.hr`
	margin: 1.5rem 0;
`;

const Assignments = () => <Wrapper>
		<h1>Assignments</h1>
		<StatusContextConsumer>
			{status => {
				if (status.assignments === undefined) return 'The assignments are not yet available.';
				return (
					<Wrapper>
						<ProjectContextConsumer>
							{projects => <Table>
								{projects.map(project => {
									let groups = status.assignments[project.id];
									let quantifier = 'Group';
									if (groups.length === 0) {
										quantifier = 'No groups'
									} else if (groups.length > 1) {
										quantifier += 's'
									}
									return <tr key={project.id}>
										<td>
											<Link to={'/projects/' + project.id}>
												{project.name}
											</Link>
										</td>
										<td>
											{quantifier} {groups.join(', ')}
										</td>
									</tr>
								})}
							</Table>}
						</ProjectContextConsumer>
						<Hr/>
						<Table>
							{[...Array(Config.selectionSize).keys()].map(i => {
								const statsLine = status.statistics[i];
								let count = (statsLine === undefined) ? 0 : statsLine;
								return (
									<tr>
										<td>{i + 1}<Sup>{ordinalSuffix(i + 1)}</Sup> preference</td>
										<td>{count} groups</td>
									</tr>
								);
							})}
						</Table>

					</Wrapper>
				);
			}}
		</StatusContextConsumer>
	</Wrapper>
;

export default Assignments;