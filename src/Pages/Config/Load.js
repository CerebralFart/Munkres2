import React from 'react';
import Socket from '../../Socket';
import {Redirect} from "react-router-dom";
import {Wrapper} from "../../Util";
import {notify} from "../../Notification";

class ConfigLoad extends React.Component {
	state = {
		loading: false,
		done: false,
		rep: 1
	};
	timeout;
	input;

	constructor(props) {
		super(props);
		this.onChange = this.onChange.bind(this);
		this.openDialog = this.openDialog.bind(this);
		this.queueRepetitionUpdate();
	}

	onChange(evt) {
		let reader = new FileReader();
		reader.onload = () => {
			let text = reader.result;
			Socket.registerSingleMessageHandler('admin-import', () => {
				this.setState({
					done: true
				})
			});
			try {
				const data = JSON.parse(text);
				Socket.sendCommand('admin-import', data);
			} catch (e) {
				notify('Sorry, we couldn\'t parse that');
				this.setState({loading: false});
			}
		};
		reader.readAsText(evt.target.files[0]);
		this.setState({
			loading: true
		});
	}

	openDialog() {
		if (this.state.loading) return;
		this.input.click();
	}

	queueRepetitionUpdate() {
		this.timeout = window.setInterval(() => {
			let rep = this.state.rep;
			if (rep === 3) {
				rep = 1;
			} else {
				rep++;
			}
			this.setState({rep});
		}, 500);
	}

	componentDidMount() {
		window.setTimeout(this.openDialog, 50);
	}

	componentWillUnmount() {
		if (this.timeout !== undefined) {
			window.clearInterval(this.timeout);
		}
	}

	render() {
		if (this.state.done) return <Redirect to='/admin/config'/>;
		if (this.state.loading) return <h1>Loading{'.'.repeat(this.state.rep)}</h1>;
		return <Wrapper>
			<h1>Please select <a onClick={this.openDialog}>a file</a></h1>
			<input type='file' accept='text/*'
				   onChange={this.onChange}
				   ref={el => this.input = el}
				   style={{display: 'none'}}
			/>
		</Wrapper>
	}
}

export default ConfigLoad;