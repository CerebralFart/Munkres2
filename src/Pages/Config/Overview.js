import React from 'react';
import styled from 'styled-components';
import {debounce, Wrapper} from "../../Util";
import {StatusContextConsumer} from "../../StatusContext";
import MenuButton from "../../Menu/Button";
import {withRouter} from "react-router-dom";
import Socket from '../../Socket'

const CenterElem = styled.div`
	text-align: center;
`;

const ButtonElem = styled.div`
	width: 100pt;
	display: inline-block;
`;

const Hr = styled.hr`
	display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
`;

const Table = styled.table`
	& td:first-child {
		font-weight: bold;
		padding-right: 15pt;
	}
`;

class Panel extends React.Component {
	state = {
		processing: false,
		title: "",
		voting: 10
	};

	constructor(props) {
		super(props);
		this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);
		this.processProps = this.processProps.bind(this);
		this.saveTitle = this.saveTitle.bind(this);
		this.saveInterval = this.saveInterval.bind(this);
		this.processProps(props);
	}

	componentWillReceiveProps(props) {
		this.processProps(props);
	}

	processProps(props) {
		setTimeout(() => {
			this.setState({
				title: props.status.title || "",
				voting: (props.status.votingInterval / 60000) || 10
			})
			;
		});
	}

	saveTitle(evt) {
		let title = evt.target.value;
		this.setState({title});
		debounce(() => {
			Socket.registerSingleMessageHandler('admin-set', response => {
				console.log(response);
				//TODO
			});
			this.setState({processing: true});
			Socket.sendCommand('admin-set', {key: 'title', value: title});
		}, 1000);
	}

	saveInterval(evt) {
		let voting = evt.target.value;
		this.setState({voting});
		debounce(() => {
			Socket.registerSingleMessageHandler('admin-set', response => {
				console.log(response);
				//TODO
			});
			this.setState({processing: true});
			Socket.sendCommand('admin-set', {key: 'votingInterval', value: voting * 60000});
		}, 1000);
	}

	render() {
		return <Wrapper>
			<h1>Configuration</h1>

			<StatusContextConsumer>{status => {
				if (status.status === 'initializing') {
					return <Wrapper>
						<CenterElem>
							<Button to='/admin/config/save'>Download</Button>
							<Button to='/admin/config/load'>Upload</Button>
						</CenterElem>
						<Hr/>
						<Table>
							<tbody>
							<tr>
								<td>Title:</td>
								<td><input value={this.state.title} placeholder="Munkres2" onChange={this.saveTitle}/>
								</td>
							</tr>
							<tr>
								<td>Voting time:</td>
								<td><input value={this.state.voting} placeholder={10} onChange={this.saveInterval}/>
								</td>
							</tr>
							</tbody>
						</Table>
					</Wrapper>
				}
				return <i>You can't change configuration after opening the voting</i>;
			}}</StatusContextConsumer>
		</Wrapper>
	};
}

const Button = withRouter(({history, children, to}) => (
	<MenuButton onClick={() => history.push(to)} liElem={ButtonElem}>
		{children}
	</MenuButton>
));

const ConfigOverview = () => (
	<StatusContextConsumer>
		{status => <Panel status={status}/>}
	</StatusContextConsumer>
);

export default ConfigOverview;