import React from 'react';
import fileDownload from 'js-file-download';
import Socket from '../../Socket';
import {Redirect} from "react-router-dom";

class ConfigSave extends React.Component {
	state = {
		done: false
	};

	constructor(props) {
		super(props);
		Socket.registerSingleMessageHandler('admin-export', response => {
			let string = JSON.stringify(response.response);
			fileDownload(string, 'munkres2.json');
			this.setState({done: true})
		});
		Socket.sendCommand('admin-export');
	}

	render() {
		if (this.state.done) {
			return <Redirect to='/admin/config'/>;
		} else {
			return <h1>Downloading</h1>;
		}
	}
}

export default ConfigSave;