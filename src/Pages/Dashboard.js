import React from 'react';
import styled from 'styled-components';
import {Wrapper} from "../Util";
import {projectSymbol} from "../Project/Context";
import {Link} from "react-router-dom";
import {preferencesSymbol} from "../Preferences";
import {assignmentSymbol} from "../AssignmentContext";
import {userSymbol} from "../UserContext";
import {CombinedContextConsumer} from "../CombinedContext";

const P = styled.p`
	margin-bottom: 1.4em;
`;

const Dashboard = () => {
	return <Wrapper>
		<h1>Dashboard</h1>
		<CombinedContextConsumer contexts={[
			userSymbol,
			projectSymbol,
			preferencesSymbol,
			assignmentSymbol
		]}>
			{(user, projects, preferences, assignment) => {
				if (user.isAdmin) {
					//TODO
					return <P>GROUPS&PROJECTS</P>
				} else {
					let projectLine = projects.length === 0 ?
						'There are no projects yet :(' :
						<Wrapper>
							There are {projects.length} projects available,
							you can take a look <Link to='/projects'>here</Link>,
							and indicate <Link to='/preferences'>your preferences</Link>.
						</Wrapper>;
					let preferencesLine = preferences.length > 0 ?
						'Please indicate your preferences as soon as possible, then we can assign you to a project.' :
						'You have indicated your preferences! We will determine your assignment as soon as possible.';
					let assignmentLine = 'Once all preferences are known, the system will determine what project will be the best match for you and your group.';
					if (assignment !== null) {
						const filtered = projects.filter(project => project.id === assignment);
						if (filtered.length !== 1) return 'Something went wrong. :(';
						const project = filtered[0];

						assignmentLine = <Wrapper>
							You have been assigned to&nbsp;
							<Link to={'/projects/' + project.id}>
								{project.name}
								{project.name !== project.by && (' by ' + project.by)}
							</Link>
							.
						</Wrapper>
					}

					return <Wrapper>
						<P>{projectLine}</P>
						<P>{preferencesLine}</P>
						<P>{assignmentLine}</P>
						<P>If you have questions, ask your teacher.</P>
					</Wrapper>
				}
			}}
		</CombinedContextConsumer>
		<P>
			If you want to contribute, you can do so on&nbsp;
			<a target="_blank" rel="noopener noreferrer"
			   href="https://github.com/CerebralFart/Munkres2">
				Github <i className="fab fa-github"/>
			</a>.
		</P>
	</Wrapper>;
};

export default Dashboard;

