import React from 'react';
import styled from 'styled-components';
import {Wrapper} from "../../Util";
import Socket from '../../Socket';

const List = styled.ol`
	list-style-type: disc;
`;

const Item = styled.li``;

class AdminOverview extends React.Component {
	state = {
		groups: null,
		count: 0
	};

	constructor(props) {
		super(props);

		this.updateCount = this.updateCount.bind(this);
		this.regenGroups = this.regenGroups.bind(this);
	}

	componentDidMount() {
		Socket.registerSingleMessageHandler('admin-groups', ({response}) => {
			this.setState({
				groups: response,
				count: response.length
			});
		});
		Socket.sendCommand('admin-groups');
	}

	updateCount(event) {
		this.setState({count: event.target.value});
	}

	regenGroups() {
		Socket.registerSingleMessageHandler('admin-groups-set', () => {
			this.componentDidMount();
		});
		Socket.sendCommand('admin-groups-set', {count: this.state.count});
	}

	render() {
		const {groups, count} = this.state;
		return (
			<Wrapper>
				<h1>Groups - Admin Overview</h1>
				{groups === null && 'Loading...'}
				<List>
					{groups !== null && groups.map((group, idx) =>
						<Item key={group.id}>
							Group {idx + 1}: {group.code}
						</Item>
					)}
				</List>
				<br/><br/>
				<div className="input-group input-group-lg">
					<div className="input-group-prepend">
						<span className="input-group-text">Desired amount</span>
					</div>
					<input type="text" className="form-control" value={count} onChange={this.updateCount}/>
					<div className="input-group-append">
						<button className="input-group-text" onClick={this.regenGroups}>Update</button>
					</div>
				</div>
			</Wrapper>
		);
	}
}

export default AdminOverview;