import React from 'react';
import styled from 'styled-components';
import {Wrapper} from "../Util";
import {projectSymbol} from "../Project/Context";
import {preferencesSymbol, setPreference} from "../Preferences";
import Card from "../Project/Card";
import {Container, Draggable} from "react-smooth-dnd";
import {assignmentSymbol} from "../AssignmentContext";
import {CombinedContextConsumer} from "../CombinedContext";

const Column = styled.div`
	width: calc(50% - 20pt);
	display: inline-block;
	vertical-align: top;
	margin: 10pt;
	
	& h3 {
		text-align: center;
		font-size: 1.4em;
		font-weight: 300;
	}
`;

const Dropzone = styled.div`
	position: relative;
	width: 100%;
	height: 150pt;
	margin: 10pt 0;
	
	display: inline-block;
	background-color: lightgrey;
	color: rgb(227, 230, 232);
	font-weight: bold;
	text-align: center;
	line-height:150pt;
	font-size: 5em;
`;

const dropSelect = ({addedIndex, removedIndex, droppedElement}) => {
	if (addedIndex === null) return;
	const projectId = parseInt(droppedElement.dataset.project, 10);
	if (removedIndex !== null) {
		setPreference(projectId, null);
	}
	setPreference(
		projectId,
		addedIndex
	);
};

const dropRemove = ({addedIndex, droppedElement}) => {
	if (addedIndex === null) return;
	setPreference(
		parseInt(droppedElement.dataset.project, 10),
		null
	)
};

const MightBeDraggable = ({disabled, children, ...props}) => {
	if (disabled) {
		return <div {...props}>{children}</div>;
	} else {
		return <Draggable {...props}>{children}</Draggable>;
	}
};

const Preferences = () => {
	return <Wrapper>
		<h1>Preferences</h1>
		<CombinedContextConsumer contexts={[
			projectSymbol,
			preferencesSymbol,
			assignmentSymbol
		]}>
			{(projects, preferences, assignment) => {
				return <Wrapper>
					<Column>
						<h3>Selected</h3>
						<Container
							groupName='project'
							onDrop={dropSelect}

						>
							{preferences.map(projectId => {
								const project = projects.filter(project => project.id === projectId)[0];
								return (
									<MightBeDraggable
										key={projectId}
										disabled={assignment !== null}
									>
										<Card project={project}/>
									</MightBeDraggable>
								);
							})}
							{preferences.length < 3 && assignment === null && <Dropzone>+</Dropzone>}
						</Container>
					</Column>
					<Column>
						<h3>Not selected</h3>
						<Container
							groupName='project'
							onDrop={dropRemove}
						>
							{projects.filter(project => preferences.indexOf(project.id) === -1).map(project =>
								<MightBeDraggable
									key={project.id}
									disabled={assignment !== null}
								>
									<Card project={project}/>
								</MightBeDraggable>
							)}
						</Container>
					</Column>
				</Wrapper>;
			}}
		</CombinedContextConsumer>
	</Wrapper>;
};

export default Preferences;

