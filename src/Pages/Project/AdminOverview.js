import React from 'react';
import styled from 'styled-components';
import {Wrapper} from "../../Util";
import {ProjectContextConsumer} from "../../Project/Context";
import Card from "../../Project/Card";
import {Link} from "react-router-dom";

const TwoColumnLayout = styled.div`
	column-count:2;
`;


const AddButton = styled(Link)`
	position: relative;
	width: 100%;
	height: 150pt;
	margin: 10pt 0;
	
	display: inline-block;
	background-color: lightgrey;
	color: rgb(227, 230, 232);
	font-weight: bold;
	text-align: center;
	line-height:150pt;
	font-size: 5em;
`;

const AdminOverview = () => <Wrapper>
	<h1>Projects - Admin Overview</h1>
	<TwoColumnLayout>
		<ProjectContextConsumer>
			{projects => projects.map(project =>
				<Card
					key={project.id}
					link={'/admin/projects/' + project.id}
					project={project}
				/>
			)}
		</ProjectContextConsumer>
		<AddButton to={'/admin/projects/new'}>+</AddButton>
	</TwoColumnLayout>
</Wrapper>;

export default AdminOverview;