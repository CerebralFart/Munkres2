import React from 'react';
import styled from 'styled-components';
import {Wrapper} from "../../Util";
import {ProjectContextConsumer} from "../../Project/Context";

const Name = styled.h1``;
const By = styled.h3`
	text-align: center;
	font-size: 1.4rem;
	font-style: italic;
`;
const Info = styled.div`
	padding-top: 0.5em;
	font-size: 0.8em;
	text-align: center;
`;
const Description = styled.div`
	& p:first-of-type {
		margin-top: 2rem;
	}
	
	& p {
		padding: 1rem 0;
	}
`;

const Details = ({projectId}) => <ProjectContextConsumer>
	{projects => {
		projectId = parseInt(projectId, 10);
		const filteredProjects = projects.filter(p => p.id === projectId);

		if (filteredProjects.length === 0) {
			return <h1>Couldn't find that project</h1>
		} else {
			const project = filteredProjects[0];
			return <Wrapper>
				<Name>{project.name}</Name>
				{project.name === project.by || <By>by {project.by}</By>}
				<Info title={project.places + ' places available'}><i className={'fa fa-users'}/> {project.places}
				</Info>
				<Description>{project.description.split('\n').map((p, idx) => <p key={idx}>{p}</p>)}</Description>
			</Wrapper>;
		}

	}}
</ProjectContextConsumer>;

export default Details;