import React from 'react';
import {ProjectContextConsumer} from "../../Project/Context";
import {Wrapper} from "../../Util";
import {StatusContextConsumer} from "../../StatusContext";
import styled from "styled-components";
import Socket from '../../Socket';
import {withRouter} from "react-router-dom";

const TextArea = styled.textarea`
	width: 100%;
	height: 15em;
`;

const P = styled.p`
	margin-bottom: 1.4em;
`;

export const EditForm = ({editable, project, handler, error}) => {
	if (!project) {
		project = {
			name: "",
			by: "",
			places: undefined,
			description: ""
		};
	}

	const clickHandler = () => {
		const form = document.forms[0];
		let data = {};
		for (let i = 0; i < form.length; i++) {
			const name = form[i].name;
			const value = form[i].value;
			if (name === 'places') {
				data.places = parseInt(value, 10);
			} else if (name !== '') {
				data[name] = value;
			}
		}
		handler(data);
	};
	return (
		<form>
			<P>
				<b>Name:</b><br/>
				<input name='name' defaultValue={project.name} disabled={!editable}/>
			</P>
			<P>
				<b>By:</b><br/>
				<input name='by' defaultValue={project.by} disabled={!editable}/>
			</P>
			<P>
				<b>Places:</b><br/>
				<input name='places' type="number" defaultValue={project.places}
					   disabled={!editable}/>
			</P>
			<P>
				<b>Description:</b><br/>
				<TextArea name='description' disabled={!editable} defaultValue={project.description}/>
			</P>
			{error && <P>{error}</P>}
			<button type='button' onClick={clickHandler}>Save</button>
		</form>
	);
};

class Edit extends React.Component {
	status = {
		id: null,
		error: false
	};

	history;

	constructor(props) {
		super(props);
		this.history = props.history;
		this.state = {
			id: parseInt(props.match.params.id, 10)
		};
		this.clickHandler = this.clickHandler.bind(this);
		this.removeHandler = this.removeHandler.bind(this);
	}


	clickHandler(data) {
		data.id = this.state.id;
		Socket.registerSingleMessageHandler('admin-edit-project', ({response}) => {
			if (response.status === 'okay') {
				this.history.push('/admin/projects');
				Socket.sendCommand('projects')
			} else {
				this.setState({error: true})
			}
		});
		Socket.sendCommand('admin-edit-project', data)
	};

	removeHandler() {
		Socket.registerSingleMessageHandler('admin-delete-project', () => {
			Socket.sendCommand('projects');
		});
		Socket.sendCommand('admin-delete-project', {id: this.state.id});

		this.history.push('/admin/projects');
	}

	render() {
		const {id} = this.state;
		return <Wrapper>
			<h1>Projects - Edit</h1>
			<ProjectContextConsumer>
				{projects => {
					const filtered = projects.filter(project => project.id === id);
					if (filtered.length > 0) {
						const project = filtered[0];
						return <StatusContextConsumer>
							{status => {
								const editable = status.status === 'initializing';
								return <EditForm
									editable={editable}
									project={project}
									handler={this.clickHandler}
									error={this.state.error ? 'Something\'s not right :(' : null}
								/>
							}}
						</StatusContextConsumer>
					} else {
						return 'Sorry, we couldn\'t find that project. :('
					}
				}}
			</ProjectContextConsumer>
			<button onClick={this.removeHandler}>Remove</button>
		</Wrapper>
	}
}

export default withRouter(Edit);