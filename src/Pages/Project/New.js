import React from 'react';
import {Wrapper} from "../../Util";
import {EditForm} from "./Edit";
import Socket from '../../Socket';
import {withRouter} from "react-router-dom";

class New extends React.Component {
	state = {
		error: false
	};

	render() {
		const {error} = this.state;
		const {history} = this.props;
		return <Wrapper>
			<h1>Projects - New</h1>
			<EditForm
				editable={true}
				handler={(data) => {
					Socket.registerSingleMessageHandler('admin-add-project', ({response}) => {
						if (response.status === 'okay') {
							Socket.sendCommand('projects');
							history.push('/admin/projects');
						} else {
							this.setState({error: true});
						}
					});
					Socket.sendCommand('admin-add-project', data);
				}}
				error={error ? 'There\'s still some information missing :(' : null}
			/>
		</Wrapper>
	}
}

export default withRouter(New);