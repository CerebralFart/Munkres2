import React from 'react';
import styled from 'styled-components';
import {Wrapper} from "../../Util";
import {ProjectContextConsumer} from "../../Project/Context";
import Card from '../../Project/Card';

const GridRoot = styled.ul`
    column-count: 2;
    column-gap: 20pt;
    column-fill: balance;
    
    @media screen and (max-width: 768px) {
    	column-count: 1
    }
    
    @media screen and (min-width: 1170px) {
    	column-count: 3
	}
`;

const Overview = () => <Wrapper>
	<h1>Projects - Overview</h1>
	<GridRoot>
		<ProjectContextConsumer>
			{projects => projects.map(project =>
				<Card
					key={project.id}
					link={'/projects/' + project.id}
					project={project}
				/>
			)}
		</ProjectContextConsumer>
	</GridRoot>
</Wrapper>;

export default Overview;