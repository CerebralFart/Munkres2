import React from 'react';
import Socket from "./Socket";
import Config from "./Config";
import {registerContext} from "./CombinedContext";
import {isAdmin} from "./UserContext";

export const preferencesSymbol = Symbol('preferences');
const Context = React.createContext(preferencesSymbol);

let proxyObject;

export const setPreference = (project, index) => {
	if (index === null) {
		proxyObject.splice(proxyObject.indexOf(project), 1);
	} else {
		proxyObject.splice(index, 0, project);
	}
	if (proxyObject.length > Config.selectionSize) {
		delete proxyObject[3];
	}
};

const setPreferencesList = (preferences) => {
	while (proxyObject.length) {
		proxyObject.pop();
	}
	for (let i = 0; i < preferences.length; i++) {
		proxyObject[i] = preferences[i];
	}
	while (proxyObject.length > Config.selectionSize) {
		delete proxyObject[Config.selectionSize];
	}
};

export class PreferencesContextProvider extends React.Component {
	timeout = null;
	state = {
		preferences: []
	};

	constructor(props) {
		super(props);

		this.publishDisabled = false;

		this.queueUpdate = this.queueUpdate.bind(this);
		this.loadPref = this.loadPref.bind(this);
	}

	queueUpdate() {
		if (this.publishDisabled) return;
		if (this.timeout !== null) {
			clearTimeout(this.timeout);
		}
		this.timeout = setTimeout(() => {
			this.loadPref();
			Socket.sendCommand('set-preferences', proxyObject.slice());
		});
	}

	loadPref() {
		this.setState({preferences: proxyObject.slice()});
	}

	componentWillMount() {
		proxyObject = new Proxy([], {
			deleteProperty: (target, property) => {
				target.splice(property, 1);
				this.queueUpdate();
				return true;
			},
			set: (target, property, value) => {
				target[property] = value;
				this.queueUpdate();
				return true;
			}
		});

		this.loadPref();

		Socket.registerMessageHandler('preferences', data => {
			setPreferencesList(data.response);
		});
		Socket.sendCommand('preferences');
	}

	componentWillUnmount() {
		Socket.removeMessageHandler('preferences');
		proxyObject = proxyObject.slice();
	}

	render() {
		return <Context.Provider value={this.state.preferences}>
			{this.props.children}
		</Context.Provider>
	}
}

export const PreferencesContextConsumer = Context.Consumer;

registerContext(preferencesSymbol, PreferencesContextConsumer, PreferencesContextProvider, () => !isAdmin());