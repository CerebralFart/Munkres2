import styled from "styled-components";
import React from "react";


const Div = styled.div`
	position: absolute;
	width: 100%;
	height: 100%;
	background-color: white;
	z-index: 5;
	opacity: 0.7;
	
	& i{
		position: absolute;
		top: 40pt;
		left: 90pt;
		font-size: 60pt;
	}
`;

const ProcessingOverlay = () => <Div><i className='fas fa-spinner fa-spin fa-5x'/></Div>;

export default ProcessingOverlay;