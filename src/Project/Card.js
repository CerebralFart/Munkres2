import React from 'react';
import {Link} from "react-router-dom";
import {generateHSL} from "../Util";
import styled, {css} from "styled-components";

const MaybeLink = ({link, children, titleOnly, seed, ...props}) => {
	if (link) {
		return <Link to={link} {...props}>{children}</Link>;
	} else {
		return <div {...props}>{children}</div>;
	}
};

const Item = styled(MaybeLink)`
	position: relative;
	width: 100%;
	height: 150pt;
	margin: 10pt 0;
	
	display: inline-block;
	background-color: ${props => generateHSL(props.seed)};
	color: rgb(62, 69, 76);
	
	& h3 {
		position: absolute;
		top: 60pt;
		width: 100%;
		margin: 0 auto;
		text-align: center;
		font-size: 2.5rem;
		font-weight: 300;
		${props => !props.titleOnly && css`
			top: 15pt;
		`}
	}
	
	& h5 {
		position: absolute;
		top: 130pt;
		width: 100%;
		text-align: center;
		font-style: italic;
	}
`;

const Card = ({project, link}) => (
	<Item
		key={project.id}
		seed={project.id}
		data-project={project.id}
		titleOnly={project.name === project.by}
		link={link || null}
	>
		<h3>{project.name}</h3>
		{project.name === project.by || <h5>by {project.by}</h5>}
	</Item>
);

export default Card;