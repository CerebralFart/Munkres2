import React from 'react';
import Socket from "../Socket";
import {registerContext} from "../CombinedContext";

export const projectSymbol = Symbol('project');
const ProjectContext = React.createContext(projectSymbol);

export class ProjectContextProvider extends React.Component {
	state = {
		projects: []
	};

	constructor(props) {
		super(props);
		Socket.registerMessageHandler('projects', (data) => this.setState({projects: data.response}));
		Socket.sendCommand('projects');
	}

	render() {
		return (
			<ProjectContext.Provider value={this.state.projects}>
				{this.props.children}
			</ProjectContext.Provider>
		)
	}
}

export const ProjectContextConsumer = ProjectContext.Consumer;

registerContext(projectSymbol, ProjectContextConsumer, ProjectContextProvider);