import React from 'react';
import {isLocalHost} from "./Util";
import {registerContext} from "./CombinedContext";
import {StatusContextConsumer, StatusContextProvider} from "./StatusContext";

export const socketSymbol = Symbol('socket');
const SocketContext = React.createContext(socketSymbol);

class SocketClass {
	clientSecret = null;
	socket;
	isOpen = false;
	handlers = {
		'ping': () => {
		},
	};
	queue = [];

	constructor(address) {
		this.socket = new WebSocket('ws://' + address);
		this.socket.onopen = () => {
			this.isOpen = true;
		};

		this._handleQueue = this._handleQueue.bind(this);
		this._handleMessage = this._handleMessage.bind(this);

		this.socket.onmessage = this._handleMessage;
		window.setInterval(this._handleQueue, 50);
	}

	sendCommand(command, data) {
		if (data === undefined) {
			data = []
		}
		this.queue.push([command, data])
	}

	registerMessageHandler(command, handler) {
		this.handlers[command] = handler;
	}

	registerSingleMessageHandler(command, handler) {
		this.registerMessageHandler(command, data => {
			handler(data);
			this.removeMessageHandler(command);
		})
	}

	removeMessageHandler(command) {
		delete this.handlers[command];
	}

	hasClientSecret() {
		return this.clientSecret !== null;
	}

	setClientSecret(secret) {
		this.clientSecret = secret
	}

	_handleQueue() {
		if (this.isOpen && this.queue.length > 0) {
			const message = this.queue.shift();
			let data = {
				command: message[0],
				params: message[1]
			};
			if (this.clientSecret !== null) {
				data.auth = this.clientSecret;
			}
			this.socket.send(JSON.stringify(data));
		}
	}

	_handleMessage(evt) {
		const commands = Object.keys(this.handlers);
		const data = JSON.parse(evt.data);
		if (commands.indexOf(data.command) === -1) {
			console.warn('Unrecognized command', data);
		} else {
			this.handlers[data.command](data);
		}
	}
}

let location = window.location.host;
if (isLocalHost) {
	location = 'localhost:5000';//TODO functional but not nice
} else if (location.startsWith('192.168')) {
	location = location.split(':')[0] + ':5000';//TODO allow dynamic ports
}

const Socket = new SocketClass(location);
export default Socket;

export class SocketStatusProvider extends React.Component {
	pingTimeout = 5000;
	timeout = null;
	interval = null;
	state = {
		connected: true
	};

	constructor(props) {
		super(props);
		Socket.registerMessageHandler('ping', () => {
			if (this.timeout !== null) {
				window.clearTimeout(this.timeout);
			}
			this.setState({connected: true});
			this.timeout = window.setTimeout(() => {
				this.setState({connected: false});
			}, 1.5 * this.pingTimeout)
		});
		this.interval = window.setInterval(() => {
			console.log('ping');
			Socket.sendCommand('ping');
		}, this.pingTimeout);
		Socket.sendCommand('ping');

		this.onClose = this.onClose.bind(this);

		Socket.socket.onerror = this.onClose;
		Socket.socket.onclose = this.onClose;
	}

	onClose() {
		this.setState({connected: false});
		if (this.interval !== null) {
			window.clearInterval(this.interval);
			this.interval = null;
		}
	}

	componentWillUnmount() {
		Socket.removeMessageHandler('ping');
		window.clearInterval(this.interval);
	}

	render() {
		const {connected} = this.state;
		const {children} = this.props;

		return (
			<SocketContext.Provider value={connected}>
				{children}
			</SocketContext.Provider>
		)
	}
}

export const SocketStatusConsumer = SocketContext.Consumer;

registerContext(socketSymbol, StatusContextConsumer, StatusContextProvider);