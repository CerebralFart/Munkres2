import React from 'react';
import Socket from './Socket';
import {registerContext} from "./CombinedContext";

export const statusSymbol = Symbol('status');
const StatusContext = React.createContext(statusSymbol);

export class StatusContextProvider extends React.Component {
	state = {};

	constructor(props) {
		super(props);
		Socket.registerMessageHandler('status', message => {
			const response = message.response;
			this.setState(response);
			document.getElementsByTagName("title")[0].innerHTML = (response.title !== undefined && response.title !== null && response.title !== "") ? response.title : 'Munkres2 Project Assignment';
		});
		Socket.sendCommand('status');
	}

	render() {
		console.log('Status updated', this.state);
		return (
			<StatusContext.Provider value={this.state}>
				{this.props.children}
			</StatusContext.Provider>
		)
	}
}

export const StatusContextConsumer = StatusContext.Consumer;

registerContext(statusSymbol, StatusContextConsumer, StatusContextProvider);