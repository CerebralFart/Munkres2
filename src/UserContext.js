import React from 'react';
import {registerContext} from "./CombinedContext";

export const userSymbol = Symbol('user');
const UserContext = React.createContext(userSymbol);

let mountedInstances = [];

let user = {
	isAdmin: false
};

export const isAdmin = () => user.isAdmin;

export const updateUserContext = (type) => {
	user.isAdmin = type === 'admin';

	setTimeout(() => {
		mountedInstances.forEach(instance => {
			instance.setState({user})
		});
	});
};

export class UserContextProvider extends React.Component {
	state = {user};

	componentDidMount() {
		mountedInstances.push(this);
		this.setState({user});
	}

	componentWillUnmount() {
		mountedInstances.splice(mountedInstances.indexOf(this), 1);
	}

	render() {
		return (
			<UserContext.Provider value={this.state.user}>
				{this.props.children}
			</UserContext.Provider>
		)
	}
}

export const UserContextConsumer = UserContext.Consumer;

registerContext(userSymbol, UserContextConsumer, UserContextProvider);