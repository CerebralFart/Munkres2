import React from "react";

export const generateHSL = (seed) => `hsl(${seed * 149 % 360}, 100%, 87.5%)`;

export const Function = ({children}) => children();
export const Wrapper = ({children}) => children;

export class SafeComponent extends React.Component {
	mounted;

	componentDidMount() {
		this.mounted = true;
	}

	componentWillUnmount() {
		this.mounted = false;
	}

	setState(values) {
		if (this.mounted) {
			super.setState(values);
		}
	}
}

export const isLocalHost =
	window.location.hostname === 'localhost' ||
	// [::1] is the IPv6 localhost address.
	window.location.hostname === '[::1]' ||
	// 127.0.0.1/8 is considered localhost for IPv4.
	window.location.hostname.match(
		/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
	);

export const ordinalSuffix = (i) => {
	const j = i % 10;
	const k = i % 100;
	if (k > 10 && k < 30)
		return 'th';
	if (j === 1)
		return 'st';
	if (j === 2)
		return 'nd';
	if (j === 3)
		return 'rd';
	return 'th';
};

let timeout;
export const debounce = (func, wait) => {
	clearTimeout(timeout);
	timeout = setTimeout(function () {
		timeout = null;
		func();
	}, wait);
};

export const restrictInt = (actual, min, max) =>
	Math.min(Math.max(actual, min), max);

const r = /([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})/i;
export const shadeColor = (color, amount) => {
	amount += 1;
	let components = r.exec(color);
	components.shift();
	let darkened = components.map(component => {
		let value = parseInt(component, 16);
		let adjusted = restrictInt(value * amount, 0, 255);
		let newComponent = Math.round(adjusted).toString(16);
		return ("00" + newComponent).slice(-2);
	});
	return '#' + darkened.join('');
};
